/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.user.consoleapp.presentation;

import eapli.ecafeteria.domain.cafeteria.BookMeal;
import eapli.framework.util.DateTime;
import eapli.framework.visitor.Visitor;


/**
 *
 * @author PC
 */
public class DeliveredMealPrinter implements Visitor<BookMeal> {

    @Override
    public void visit(BookMeal visitee) {
        System.out.printf("%-17s%-20s%-10s\n", visitee.MealSelected().dish().name(),
                "Date: "+visitee.MealSelected().date(),
                "Meal Type: "+visitee.MealSelected().mealType());
    }
}
