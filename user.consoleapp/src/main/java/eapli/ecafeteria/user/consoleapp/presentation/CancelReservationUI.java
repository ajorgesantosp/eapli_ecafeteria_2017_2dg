package eapli.ecafeteria.user.consoleapp.presentation;


import eapli.ecafeteria.application.cafeteria.CancelReservationController;
import eapli.framework.application.Controller;
import eapli.framework.presentation.console.AbstractUI;

/**
 *
 * @author Marcia
 */
public class CancelReservationUI extends AbstractUI {

    private final CancelReservationController Controller = new CancelReservationController();

    protected Controller controller() {
        return this.Controller;
    }
    
    @Override
    protected boolean doShow() {
        return Controller.StartCancelReservation();
    }

    @Override
    public String headline() {
        return "Cancel Reservation";
    }
}
