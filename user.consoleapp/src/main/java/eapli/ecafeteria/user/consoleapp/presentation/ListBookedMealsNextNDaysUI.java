/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.user.consoleapp.presentation;

import eapli.ecafeteria.Application;
import eapli.ecafeteria.application.cafeteria.ListBookedMealsNextNDaysController;
import eapli.ecafeteria.domain.authz.Username;
import eapli.ecafeteria.domain.cafeteria.BookMeal;
import eapli.ecafeteria.domain.cafeteria.CafeteriaUser;
import eapli.ecafeteria.persistence.CafeteriaUserRepository;
import eapli.ecafeteria.persistence.PersistenceContext;
import eapli.framework.application.Controller;
import eapli.framework.presentation.console.AbstractUI;
import eapli.framework.util.io.Console;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

/**
 *
 * @author Andre couto
 */
public class ListBookedMealsNextNDaysUI extends AbstractUI  {

    
    private final ListBookedMealsNextNDaysController controller = new ListBookedMealsNextNDaysController();
    private Iterable<BookMeal> lbm= new ArrayList<BookMeal>();
    private CafeteriaUser cu;
    private final CafeteriaUserRepository repo = PersistenceContext.repositories().cafeteriaUsers(null);
    
    protected Controller controller() {
        return this.controller;
    }

    @Override
    protected boolean doShow() {
        //Get CafeteriaUser
        Username username = Application.session().session().authenticatedUser().id();
        cu = repo.findByUsername(username);
        //Calendar
        Calendar today = Calendar.getInstance();
        final String str_days = Console.readLine("Insert the number of days that you wich to see your booked meals!");
        final int n=Integer.parseInt(str_days);
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        System.out.println("Date --- Dish --- MealType");
        for (int i = 0; i < n; i++) {
            
            lbm=controller.listBookedMealsNextNDays(cu, today);
            System.out.println("");
            for (BookMeal bm : lbm) {
                System.out.println(sdf.format(today.getTime())+" --- "+bm.MealSelected().dish().name().toString()+" --- "+bm.MealSelected().mealType().name());
            }
            today.add(Calendar.DAY_OF_MONTH, 1);
        }
        return false;
    }

    @Override
    public String headline() {
        return "Booked Meal From the Next N Days";
    }
    
    
    
}
