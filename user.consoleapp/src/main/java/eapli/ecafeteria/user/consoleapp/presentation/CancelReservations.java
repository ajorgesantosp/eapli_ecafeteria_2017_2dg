package eapli.ecafeteria.user.consoleapp.presentation;

import eapli.cafeteria.consoleapp.presentation.ExitWithMessageAction;
import eapli.framework.actions.Action;
import eapli.framework.presentation.console.Menu;
import eapli.framework.presentation.console.MenuItem;
import eapli.framework.presentation.console.ShowUiAction;

/**
 *
 * @author Marcia
 */
public class CancelReservations extends Menu {

    private final int CANCEL_RESERVATION = 1;
    private final int EXIT_OPTION = 0;

    CancelReservations() {
        super("Cancel Reservation");
        this.cancelReservation();
    }

    private void cancelReservation() {
        add(new MenuItem(CANCEL_RESERVATION, "Cancel Reservation", new ShowUiAction(new CancelReservationUI())));
        add(new MenuItem(EXIT_OPTION, "Exit", new ExitWithMessageAction()));
    }

}
