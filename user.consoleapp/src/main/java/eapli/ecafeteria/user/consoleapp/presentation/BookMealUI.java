/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.user.consoleapp.presentation;

import eapli.ecafeteria.Application;
import eapli.ecafeteria.application.meals.RegisterBookMealController;
import eapli.ecafeteria.domain.authz.SystemUser;
import eapli.ecafeteria.domain.cafeteria.BookMeal;
import eapli.ecafeteria.domain.cafeteria.CafeteriaUser;
import eapli.ecafeteria.domain.cafeteria.MecanographicNumber;
import eapli.ecafeteria.domain.cafeteria.OrganicUnit;
import eapli.ecafeteria.domain.meals.Meal;
import eapli.ecafeteria.domain.meals.MealType;
import eapli.framework.persistence.DataConcurrencyException;
import eapli.framework.persistence.DataIntegrityViolationException;
import eapli.framework.presentation.console.AbstractUI;
import eapli.framework.util.DateTime;
import eapli.framework.util.io.Console;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Gaspa
 */
public class BookMealUI extends AbstractUI {
    
    private RegisterBookMealController controller = new RegisterBookMealController();
    private MealType mealType;
    private Date datas;
    
    @Override
    public boolean show() {
        drawFormTitle();
        return doShow();
    }
    
    @Override
    protected boolean doShow() {
        LocalDateTime now = LocalDateTime.now();
        int day = now.getDayOfMonth();
        int year = now.getYear();
        int month = now.getMonthValue();
        
        System.out.println("Today Date: " + day + "-" + month + "-" + year);
        //int dayChoosed = Console.readInteger("Choose Day:");

        //  Date date = data(day);
        //Date date = new Date(year, month, dayChoosed);
        final String mealDay = Console.readLine("Meal day: (dd-MM-yyyy)");
        Calendar dateMeal = DateTime.parseDate(mealDay);
        
        System.out.println("Select Meal ");
        System.out.println("1 - Lunch ");
        System.out.println("2 - Dinner ");
        
        Integer op = Console.readInteger("Option:");
        if (op != null) {
            if (op == 1) {
                mealType = mealType.ALMOCO;
            } else if (op == 2) {
                mealType = mealType.JANTAR;
            }
        }
        
        ArrayList<Meal> meals = new ArrayList<>();
        meals = controller.getAvailableMeals(dateMeal.getTime(), mealType);
        
        int index = 1;
        for (Meal m : meals) {
            System.out.println(index + "-" + m.toString());
            index++;
        }
        
        int selected = Console.readInteger("Select a meal:");
        
        Meal m = meals.get(selected - 1);
        //SystemUser user = Application.session().session().authenticatedUser();

//        OrganicUnit organic = new OrganicUnit("asd", "ff", "ee");
//        MecanographicNumber meca = new MecanographicNumber("1151126");
//        CafeteriaUser cafUser = new CafeteriaUser(user, organic, meca);
//        if (controller.findAlergen(cafUser, m) == true) {
//            System.out.println("identificados alergenios ");
//
//        }
        try {
            //BookMeal booking = new BookMeal(m, cafUser);
            
            controller.booking(m);
            System.out.println("reserva concluida");
//        if (controller.refreshSaldo(cafUser, m) == false) {
//            
//        } else {
//            try {
//                
//                controller.createBooking(booking);
//                System.out.println("reserva concluida");
//            } catch (DataConcurrencyException | DataIntegrityViolationException ex) {
//                Logger.getLogger(BookMealUI.class.getName()).log(Level.SEVERE, null, ex);
//                return false;
//            }
//        }
//        return false;
        } catch (DataConcurrencyException ex) {
            Logger.getLogger(BookMealUI.class.getName()).log(Level.SEVERE, null, ex);
        } catch (DataIntegrityViolationException ex) {
            Logger.getLogger(BookMealUI.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
        
    }
    
    @Override
    public String headline() {
        return "Reservar Refição";
    }
    
}
