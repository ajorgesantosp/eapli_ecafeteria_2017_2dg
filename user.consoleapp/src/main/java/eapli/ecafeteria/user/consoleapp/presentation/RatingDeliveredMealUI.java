/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.user.consoleapp.presentation;


import eapli.ecafeteria.application.cafeteria.RatingDeliveredMealController;
import eapli.ecafeteria.domain.cafeteria.BookMeal;
import eapli.framework.application.Controller;
import eapli.framework.persistence.DataConcurrencyException;
import eapli.framework.persistence.DataIntegrityViolationException;
import eapli.framework.presentation.console.AbstractUI;
import eapli.framework.presentation.console.SelectWidget;
import eapli.framework.util.io.Console;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author PC
 */
public class RatingDeliveredMealUI extends AbstractUI {

    private RatingDeliveredMealController theController = new RatingDeliveredMealController();

    protected Controller controller() {
        return this.theController;
    }

    @Override
    protected boolean doShow() {

        final Iterable<BookMeal> deliveredMeals = this.theController.deliveredMealsByUser();

        List<BookMeal> listDelivered = new ArrayList<>();
        for (BookMeal bk : deliveredMeals) {
            listDelivered.add(bk);
        }
        if (listDelivered.isEmpty()) {
            System.out.printf("Nao ha refeiçoes entreges.\n");
            return false;
        }
        final SelectWidget<BookMeal> selector = new SelectWidget<>("Rate/Coment BookMeal",listDelivered, new DeliveredMealPrinter());
        selector.show();
        final BookMeal theDeliveredMeal = selector.selectedElement();

        int rate = 0;
        do {
            rate = Console.readInteger("insira Rating(1-5): ");
        } while (rate < 1 || rate > 5);

        final String comment = Console.readLine("Insira Comentario: ");

        try {
            this.theController.saveRating(rate, comment, theDeliveredMeal.MealSelected());
        } catch (DataIntegrityViolationException | DataConcurrencyException ex) {
            Logger.getLogger(RatingDeliveredMealUI.class.getName()).log(Level.SEVERE, null, ex);
        }

        try {
            this.theController.changeBookingMealStatus(theDeliveredMeal);
        } catch (DataConcurrencyException | DataIntegrityViolationException ex) {
            Logger.getLogger(RatingDeliveredMealUI.class.getName()).log(Level.SEVERE, null, ex);
        }

        System.out.printf("\nRating gravado.\n");

        return true;
    }

    @Override
    public String headline() {
        return "Rating Refeiçao entregue.";
    }
}
