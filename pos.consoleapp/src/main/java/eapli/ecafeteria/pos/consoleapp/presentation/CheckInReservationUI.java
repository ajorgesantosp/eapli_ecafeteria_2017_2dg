/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.pos.consoleapp.presentation;

import eapli.ecafeteria.application.meals.CheckInReservationController;
import eapli.ecafeteria.backoffice.consoleapp.presentation.meals.BookMealPrinter;
import eapli.ecafeteria.domain.cafeteria.CafeteriaUser;
import eapli.ecafeteria.domain.cafeteria.MecanographicNumber;
import eapli.ecafeteria.domain.cafeteria.BookMeal;
import eapli.framework.application.Controller;
import eapli.framework.persistence.DataConcurrencyException;
import eapli.framework.persistence.DataIntegrityViolationException;
import eapli.framework.presentation.console.AbstractUI;
import eapli.framework.presentation.console.SelectWidget;
import eapli.framework.util.io.Console;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Pedrobarbosa
 */
public class CheckInReservationUI extends AbstractUI {

    private final CheckInReservationController theController = new CheckInReservationController();

    protected Controller controller() {
        return this.theController;
    }

    @Override
    protected boolean doShow() {
        String str = Console.readLine("User's mechanographic number: ");
        final MecanographicNumber number = new MecanographicNumber(str);
        CafeteriaUser account = this.theController.getAccountID(number);
        if (account != null) {

            System.out.println("Choose a reservation: ");
            final Iterable<BookMeal> bookMeals = this.theController.listBookMeals(account);
            final SelectWidget<BookMeal> selector = new SelectWidget<>("bookmeals",bookMeals, new BookMealPrinter());
            selector.show();
            final BookMeal theBookMeal = selector.selectedElement();
            if (theBookMeal != null) {
                try {
                    this.theController.deliver(theBookMeal);
                } catch (DataConcurrencyException | DataIntegrityViolationException ex) {
                    Logger.getLogger(CheckInReservationUI.class.getName()).log(Level.SEVERE, null, ex);
                }

            }
        } else {
            System.out.println("This number is invalid\n");
        }
        return true;
    }

    @Override
    public String headline() {
        return "Pick up reservation";
    }

}
