/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.pos.consoleapp.presentation;

import eapli.ecafeteria.application.cafeteria.ChargeCardController;
import eapli.ecafeteria.domain.cafeteria.Account;
import eapli.ecafeteria.domain.cafeteria.MecanographicNumber;
import eapli.framework.application.Controller;
import eapli.framework.persistence.DataConcurrencyException;
import eapli.framework.persistence.DataIntegrityViolationException;
import eapli.framework.presentation.console.AbstractUI;
import eapli.framework.util.io.Console;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author 1120328
 */
public class ChargeCardUI extends AbstractUI {

    private final ChargeCardController theController = new ChargeCardController();

    protected Controller controller() {
        return this.theController;
    }

    @Override
    protected boolean doShow() {

        try {
            String str = Console.readLine("Number: ");
            final MecanographicNumber number = new MecanographicNumber(str);
            Account account = this.theController.getAccountID(number);

            try {
                final double saldo = Console.readInteger("Value to charge: ");
                this.theController.chargeAccount(saldo, account);
            } catch (DataConcurrencyException | DataIntegrityViolationException ex) {
                Logger.getLogger(ChargeCardUI.class.getName()).log(Level.SEVERE, null, ex);
            }

            System.out.println("Value added with success");

        } catch (Exception e) {
            System.out.println("Invalid Number");
        }

        return true;
    }

    @Override
    public String headline() {
        return "Charge Card.";
    }
}
