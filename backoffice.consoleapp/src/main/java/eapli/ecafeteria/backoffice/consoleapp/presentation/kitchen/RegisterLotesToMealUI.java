/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.backoffice.consoleapp.presentation.kitchen;

import eapli.ecafeteria.application.kitchen.RegisterLotesToMealController;
import eapli.ecafeteria.backoffice.consoleapp.presentation.meals.LotePrinter;
import eapli.ecafeteria.backoffice.consoleapp.presentation.meals.MealPrinter;
import eapli.ecafeteria.domain.kitchen.Lote;
import eapli.ecafeteria.domain.meals.Meal;
import eapli.framework.application.Controller;
import eapli.framework.presentation.console.AbstractUI;
import eapli.framework.presentation.console.SelectWidget;
import eapli.framework.util.io.Console;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Pedro
 */
public class RegisterLotesToMealUI extends AbstractUI {

    private final RegisterLotesToMealController theController = new RegisterLotesToMealController();

    protected Controller controller() {
        return this.theController;
    }

    @Override
    protected boolean doShow() {
        //APRESENTAR LISTA DE MEALS PARA UTILIZADOR ESCOLHER UMA
        int number;
        List<Lote> listLotes = new ArrayList();
        final Iterable<Meal> meal = this.theController.meals();
        final SelectWidget<Meal> selector = new SelectWidget<>("Meal:", meal, new MealPrinter());
        selector.show();
        final Meal theMeal = selector.selectedElement();

        do {
            final Iterable<Lote> lote = this.theController.lotes();
            final SelectWidget<Lote> lote_selected = new SelectWidget<>("Lote:", lote, new LotePrinter());
            lote_selected.show();
            final Lote theLote = lote_selected.selectedElement();

            //APRESENTAR LISTA LOTES EM LOOP PARA ADOCOPMAR
            number = Console.readInteger("Escreva 0 para terminar de inserir lotes, 1 para inserir mais Lotes para esta refeição ");
            listLotes.add(theLote);
        } while (number != 0);

        try {
            this.theController.indicarLotesUtilizados(theMeal, listLotes);
        } catch (final Exception e) {
            System.out.println("Erro ao indicar numero de pratos. Operação realizada sem sucesso");
        }
        return false;
    }

    @Override
    public String headline() {
        return "Register used lotes";
    }

}
