/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.backoffice.consoleapp.presentation.meals;

import eapli.ecafeteria.domain.meals.Meal;
import eapli.framework.visitor.Visitor;

/**
 *
 * @author Pedro
 */
public class MealPrinter implements Visitor<Meal> {

    @Override
    public void visit(Meal visitee) {
        System.out.println("REFEIÇÃO: " + visitee.dish().name().toString() +" TIPO DE REFEIÇÃO: " + visitee.mealType() + " DATA: "+visitee.date().toString());
    }
    
}
