/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.backoffice.consoleapp.presentation.kitchen;

import eapli.ecafeteria.application.kitchen.RegisterLotesToMealController;
import eapli.ecafeteria.application.kitchen.SearchByLoteController;
import eapli.ecafeteria.backoffice.consoleapp.presentation.meals.LotePrinter;
import eapli.ecafeteria.backoffice.consoleapp.presentation.meals.MealPrinter;
import eapli.ecafeteria.domain.kitchen.Lote;
import eapli.ecafeteria.domain.meals.Meal;
import eapli.framework.application.Controller;
import eapli.framework.presentation.console.AbstractUI;
import eapli.framework.presentation.console.SelectWidget;
import eapli.framework.util.io.Console;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Pedro
 */
public class SearchByLoteUI extends AbstractUI {

    private final SearchByLoteController theController = new SearchByLoteController();

    protected Controller controller() {
        return this.theController;
    }

    @Override
    protected boolean doShow() {
        final Iterable<Lote> lote = this.theController.lotes();
        final SelectWidget<Lote> lote_selected = new SelectWidget<>("Lote:", lote, new LotePrinter());
        lote_selected.show();
        final Lote theLote = lote_selected.selectedElement();
        List<Meal> pesquisarPorLote = new ArrayList();
        try {
            pesquisarPorLote = this.theController.pesquisarPorLote(theLote);
        } catch (final Exception e) {
            System.out.println("Erro ao pesquisar refeições com o lote indicado");
        }
        if (pesquisarPorLote.size() > 0) {
            System.out.println("Refeições que utilizaram o lote indicado em cima: ");
            for (Meal m : pesquisarPorLote) {
                System.out.println(m.toString());
            }

        } else {
            System.out.println("Nenhuma refeição encontrada que tenha usado esse lote.");
        }

        System.out.println("Prima qualquer tecla para continuar");
        try {
            System.in.read();
        } catch (IOException ex) {
        }
        return false;
    }

    @Override
    public String headline() {
        return "Search by Lote";
    }

}
