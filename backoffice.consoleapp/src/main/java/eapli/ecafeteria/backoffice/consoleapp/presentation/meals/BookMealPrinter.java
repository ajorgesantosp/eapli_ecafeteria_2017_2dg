/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.backoffice.consoleapp.presentation.meals;

import eapli.ecafeteria.domain.cafeteria.BookMeal;
import eapli.framework.visitor.Visitor;

/**
 *
 * @author Pedrobarbosa
 */
public class BookMealPrinter implements Visitor<BookMeal> {

    @Override
    public void visit(BookMeal visitee) {
        System.out.printf("%-20s%-50s%-4s\n", visitee.MealSelected().date().toString(), visitee.MealSelected().toString(), visitee.state());
    }

}
