/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.backoffice.consoleapp.presentation.meals;

import eapli.ecafeteria.domain.meals.Allergen;
import eapli.framework.visitor.Visitor;

/**
 *
 * @author Flávio Relvas
 */
public class AllergenPrinter implements Visitor<Allergen> {

    @Override
    public void visit(Allergen visitee) {
        System.out.printf("%-150s\n", visitee.name());
    }

}
