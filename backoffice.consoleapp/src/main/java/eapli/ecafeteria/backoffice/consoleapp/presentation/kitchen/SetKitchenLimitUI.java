/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.backoffice.consoleapp.presentation.kitchen;

import eapli.ecafeteria.application.kitchen.SetKitchenLimitsController;
import eapli.framework.application.Controller;
import eapli.framework.persistence.DataConcurrencyException;
import eapli.framework.persistence.DataIntegrityViolationException;
import eapli.framework.presentation.console.AbstractUI;
import eapli.framework.util.io.Console;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author <1131410 Fábio Ferreira>
 */
public class SetKitchenLimitUI extends AbstractUI {

    private final SetKitchenLimitsController theController = new SetKitchenLimitsController();

    protected Controller controller() {
        return this.theController;
    }

    @Override
    protected boolean doShow() {
        float yellowLimit = (float) Console.readDouble("Yellow Limit:");
        float redLimit = (float) Console.readDouble("Red Limit:");

        yellowLimit /= 100;
        redLimit /= 100;
        try {
            theController.updateKitchenLimit(yellowLimit, redLimit);
            System.out.println("Updated sucessfull");
        } catch (final DataIntegrityViolationException | DataConcurrencyException e) {
            System.out.println("Error Database.");
        }

        return false;
    }

    @Override
    public String headline() {
        return "Set Kitchen Limits";
    }

}
