/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.backoffice.consoleapp.presentation.meals;

import eapli.ecafeteria.domain.kitchen.Lote;
import eapli.framework.visitor.Visitor;

/**
 *
 * @author Pedro
 */
public class LotePrinter implements Visitor<Lote>{

    @Override
    public void visit(Lote visitee) {
       System.out.println("Lote: "+ visitee.id() + " Materia Prima: " + visitee.materia_prima().description());
    }
}