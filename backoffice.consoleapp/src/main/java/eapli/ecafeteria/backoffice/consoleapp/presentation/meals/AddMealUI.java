/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.backoffice.consoleapp.presentation.meals;

import eapli.ecafeteria.application.meals.AddMealController;

import eapli.ecafeteria.domain.meals.Dish;
import eapli.ecafeteria.domain.meals.Meal;
import eapli.ecafeteria.domain.meals.MealType;
import eapli.framework.application.Controller;
import eapli.framework.persistence.DataConcurrencyException;
import eapli.framework.persistence.DataIntegrityViolationException;
import eapli.framework.presentation.console.AbstractUI;
import eapli.framework.presentation.console.SelectWidget;
import eapli.framework.util.DateTime;
import eapli.framework.util.io.Console;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author andre
 */
public class AddMealUI extends AbstractUI {

    private final AddMealController theController = new AddMealController();

    protected Controller controller() {
        return this.theController;
    }

    @Override
    protected boolean doShow() {

        ArrayList<Meal> meals = new ArrayList<>();
        System.out.printf("\nCreating meals:\n");
        Integer flag = Console.readInteger("1 - Create new Meal. \n2 - Sair");
        while (flag == 1) {

            System.out.printf("\nNew Meal:\n");

            final String mealDay = Console.readLine("Meal day: (dd-MM-yyyy)");
            Calendar dateMeal = DateTime.parseDate(mealDay);
            
            final Iterable<Dish> dishes = this.theController.dishes();
            final SelectWidget<Dish> selector = new SelectWidget<>("Dishes:", dishes, new DishPrinter());
            selector.show();
            final Dish theDish = selector.selectedElement();

            System.out.printf("\nSelect a MealType:\n");
            MealType mealType = null;
            final Integer mealTypes = Console.readInteger("1 - Almoço.\n2 - Jantar");
            switch (mealTypes) {
                case 1:
                    mealType = this.theController.getMealType("ALMOCO");
                    break;
                case 2:
                    mealType = this.theController.getMealType("JANTAR");
                    break;
                default:
                    System.out.printf("Opção inválida.\n");
            }

            try {
                meals.add(this.theController.createMeal(dateMeal.getTime(), mealType, theDish));
            } catch (DataIntegrityViolationException | DataConcurrencyException ex) {
                Logger.getLogger(AddMealUI.class.getName()).log(Level.SEVERE, null, ex);

            }

            System.out.printf("\nMeal saved:\n");
            System.out.printf("Meal: " + mealDay + "|" + theDish.dishType().description()
                    + "|" + theDish.name() + "|" + mealType + "|\n");

            flag = Console.readInteger("1 - Create new Meal. \n2 - Sair");;
        }

        return true;
    }

    @Override
    public String headline() {
        return "Create Menu";
    }
}
