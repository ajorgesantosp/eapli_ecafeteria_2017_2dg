/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.backoffice.consoleapp.presentation.kitchen;

import eapli.ecafeteria.domain.kitchen.KitchenLimit;
import eapli.ecafeteria.persistence.KitchenLimitRepository;
import eapli.ecafeteria.persistence.PersistenceContext;

/**
 *
 * @author Flavio Relvas
 */
public class AlertPrinter {

    KitchenLimitRepository limitsRepository = PersistenceContext.repositories().kitchenLimits();

    public String print() {
        if (checkRedLimit()) {
            return "Red limit reached";
        } else if (checkYellowLimit()) {
            return "Yellow limit reached";
        } else {
            return "";
        }
    }

    private boolean checkRedLimit() {
        KitchenLimit limits = limitsRepository.first();
        return limits.isRedAlert();
    }

    private boolean checkYellowLimit() {
        KitchenLimit limits = limitsRepository.first();
        return limits.isYellowAlert();
    }
}
