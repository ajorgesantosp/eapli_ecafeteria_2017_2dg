/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.backoffice.consoleapp.presentation.kitchen;

import eapli.ecafeteria.application.kitchen.NumberOfDishesController;
import eapli.ecafeteria.backoffice.consoleapp.presentation.meals.MealPrinter;
import eapli.ecafeteria.domain.meals.Meal;
import eapli.framework.application.Controller;
import eapli.framework.presentation.console.AbstractUI;
import eapli.framework.presentation.console.SelectWidget;
import eapli.framework.util.io.Console;

/**
 *
 * @author Pedro
 */
public class NumberOfDishesUI extends AbstractUI {

    private final NumberOfDishesController theController = new NumberOfDishesController();

    protected Controller controller() {
        return this.theController;
    }

    @Override
    protected boolean doShow() {
        //APRESENTAR LISTA DE MEALS PARA UTILIZADOR ESCOLHER UMA
        final Iterable<Meal> meal = this.theController.meals();
        final SelectWidget<Meal> selector = new SelectWidget<>("Meal:",meal, new MealPrinter());
        selector.show();
        final Meal theMeal = selector.selectedElement();
        int number;
        
        do{
            number = Console.readInteger("Numero de pratos realmente preparados:(numero inteiro positivo) ");
        }while(number<0);

        try {
            this.theController.indicarNumeroPratos(theMeal,number);
        } catch (final Exception e) {
            System.out.println("Erro ao indicar numero de pratos. Operação realizada sem sucesso");
        }
        return false;
    }

    @Override
    public String headline() {
        return "Tell Number of dishes coocked to a meal";
    }
}
