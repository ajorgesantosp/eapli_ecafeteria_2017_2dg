/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.domain.cafeteria;

import java.util.Calendar;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author MariaInês
 */
public class BalanceTest {
    
    public BalanceTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of refreshBalance method, of class Balance.
     */
     @Test
    public void testRefreshBalance() {
        System.out.println("refreshBalance");
        double valor = 3.0;
        Calendar dayOfMovement = null;
        Balance instance = new Balance();
        instance.refreshBalance(valor);
        final Double expRes = instance.balance();
        final Double res = valor;
        assertEquals(expRes,res);
    }

    /**
     * Test of validaSaldo method, of class Balance.
     */
   @Test
   public void testValidaSaldo() {
        System.out.println("validaSaldo");
        double preco = 3.0;
        double valor = 4.0;
        Calendar dayOfMovement = null;
        Balance instance = new Balance();
        instance.refreshBalance(valor);
        boolean expResult = true;
        boolean result = instance.validaSaldo(preco);
        assertEquals(expResult, result);
    }

    /**
     * Test of balance method, of class Balance.
     */
    @Test
    public void testBalance() {
        System.out.println("balance");
        Balance instance = new Balance();
        instance.refreshBalance(10.0);
        double expResult = 10.0;
        double result = instance.balance();
        assertEquals(expResult, result,10.0);       
    }

    /**
     * Test of validateCharging method, of class Balance.
     */
    @Test
    public void testValidateCharging() {
        System.out.println("validateCharging");
        double v = 2;
        Balance instance = new Balance();
        boolean expResult = true;
        boolean result = instance.validateCharging(v);
        assertEquals(expResult, result);
    }
    
}
