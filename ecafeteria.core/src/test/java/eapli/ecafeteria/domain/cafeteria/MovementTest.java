/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.domain.cafeteria;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author MariaInês
 */
public class MovementTest {

    public MovementTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of movCharge method, of class Movement.
     */
    @Test
    public void testMovCharge() {
        System.out.println("movCharge");
        double valor = 10;
        Account ac = new Account();
        ac.refreshBalance(valor);
        Movement instance = new Movement(ac, 10.0);
        boolean expResult = true;
        boolean result = instance.movCharge();
        assertEquals(expResult, result);
    }

}
