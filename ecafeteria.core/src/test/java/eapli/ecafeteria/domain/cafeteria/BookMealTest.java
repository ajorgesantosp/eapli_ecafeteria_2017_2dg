/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.domain.cafeteria;

import eapli.ecafeteria.domain.authz.RoleType;
import eapli.ecafeteria.domain.authz.SystemUserBuilder;
import eapli.ecafeteria.domain.meals.AllergenDish;
import eapli.ecafeteria.domain.meals.Dish;
import eapli.ecafeteria.domain.meals.DishType;
import eapli.ecafeteria.domain.meals.Meal;
import eapli.ecafeteria.domain.meals.MealType;
import eapli.framework.domain.Designation;
import eapli.framework.domain.Money;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Currency;
import java.util.Date;
import java.util.HashSet;
import java.util.Locale;
import java.util.Set;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Gaspa
 */
public class BookMealTest {

    private CafeteriaUser user;

    private Calendar createdOn;
    final Set<RoleType> roles;
    private Meal meal;

    public BookMealTest() {

        createdOn = Calendar.getInstance(Locale.ITALY);
        roles = new HashSet<RoleType>();
        roles.add(RoleType.ADMIN);
        final SystemUserBuilder userBuilder = new SystemUserBuilder().withUsername("qwe").withPassword("Password1").withFirstName("qwe").withLastName("qwe")
                .withEmail("qwe@qwe.com").withCreatedOn(createdOn).withRoles(roles);
        user = new CafeteriaUser(userBuilder.build(), new OrganicUnit("qwe", "qwe", "qwe"), new MecanographicNumber("qwe"));
        meal = new Meal(new Date(123, 12, 1), MealType.ALMOCO, new Dish(new DishType("qwe", "qwe"), Designation.valueOf("qwe"), new Money(123, Currency.getInstance(Locale.ITALY)), new ArrayList<AllergenDish>()));

    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of user method, of class BookMeal.
     */
    @Test
    public void testUser() {
        System.out.println("user");

        BookMeal instance = new BookMeal(meal, user);
        CafeteriaUser expResult = user;
        CafeteriaUser result = instance.user();

        assertEquals(expResult, result);

    }

    /**
     * Test of MealSelected method, of class BookMeal.
     */
    @Test
    public void testMealSelected() {
        System.out.println("MealSelected");
        BookMeal instance = new BookMeal(meal, user);
        Meal expResult = meal;
        Meal result = instance.MealSelected();
        assertEquals(expResult, result);

    }

    /**
     * Test of equals method, of class BookMeal.
     */
    @Test
    public void testEquals() {
        System.out.println("equals");

        BookMeal obj = new BookMeal(meal, user);
        BookMeal instance = new BookMeal(meal, user);

        instance.equals(obj);
        boolean expResult = true;
        boolean result = instance.equals(obj);

        assertEquals(expResult, result);
    }

    /**
     * Test of state method, of class BookMeal.
     */
    @Test
    public void testState() {
        System.out.println("state");
        BookMeal instance = new BookMeal(meal, user);
        BookMeal.State expResult = null;
        BookMeal.State result = instance.state();
        assertEquals(expResult, result);
    }

    /**
     * Test of deliver method, of class BookMeal.
     */
//    @Test
//    public void testDeliver() {
//        System.out.println("deliver");
//        BookMeal instance = new BookMeal();
//        instance.deliver();
//    }

    /**
     * Test of reserva method, of class BookMeal.
     */
    @Test
    public void testReserva() {
        System.out.println("reserva");
        BookMeal instance = new BookMeal(meal, user);
        instance.reserva();
    }

}
