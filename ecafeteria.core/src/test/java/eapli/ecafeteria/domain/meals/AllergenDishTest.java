/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.domain.meals;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author <1131410 Fábio Ferreira>
 */
public class AllergenDishTest {

    public AllergenDishTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test(expected = IllegalStateException.class)
    public void testAllergenDishNotBeNull() {
        System.out.println("Deve conter um alergénio!");
        AllergenDish instance = new AllergenDish(null);
    }

    /**
     * Test of equals method, of class AllergenDish.
     */
    @Test
    public void testEquals() {
        System.out.println("Test Equals");
        AllergenDish a = new AllergenDish("moluscos");
        AllergenDish instance = new AllergenDish("moluscos");

        instance.equals(a);
        boolean expResult = true;
        boolean result = instance.equals(a);

        assertEquals(expResult, result);
    }

}
