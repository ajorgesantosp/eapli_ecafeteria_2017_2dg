/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.domain.meals;

import static eapli.ecafeteria.domain.meals.MealType.ALMOCO;
import static eapli.ecafeteria.domain.meals.MealType.JANTAR;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Márcia
 */
public class MealTypeTest {

    MealType ma = ALMOCO;
    MealType mj = JANTAR;

    public MealTypeTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    public void testValues() {
        System.out.println("values");
        MealType[] expResult = new MealType[]{ALMOCO, JANTAR};
        MealType[] result = MealType.values();
        assertArrayEquals(expResult, result);
    }

    /**
     * Test of valueOf method, of class MealType.
     */
    @Test

    public void testValueOf() {
        System.out.println("valueOf");
        String name = "ALMOCO";
        MealType expResult = this.ma.ALMOCO;
        MealType result = MealType.valueOf(name);
        assertEquals(expResult, result);
    }

    /**
     * Test of mealTypes method, of class MealType.
     */
    @Test

    public void testMealTypes() {
        System.out.println("mealTypes");
        MealType[] expResult = new MealType[]{ALMOCO, JANTAR};
        MealType[] result = MealType.mealTypes();
        assertArrayEquals(expResult, result);
    }
}
