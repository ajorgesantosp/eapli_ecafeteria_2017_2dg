/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.domain.meals;

import static eapli.ecafeteria.domain.meals.MealType.ALMOCO;
import static eapli.ecafeteria.domain.meals.MealType.JANTAR;
import eapli.framework.domain.Designation;
import eapli.framework.domain.Money;
import eapli.framework.util.DateTime;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Márcia
 */
public class MealTest {

    public MealTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of date method, of class Meal.
     */
    @Test
    public void testDate() {
        System.out.println("Date");
        Calendar calendar = DateTime.parseDate("14-04-2017");
        DishType disht = new DishType("Carne", "Carne");
        Designation nome = Designation.valueOf("Peru");
        Money dinheiro = Money.euros(10);
        Dish dish = new Dish(disht, nome, dinheiro, new ArrayList<>());
        Meal instance = new Meal(calendar.getTime(), MealType.ALMOCO, dish);
        Date expResult = calendar.getTime();
        Date result = instance.date();
        assertEquals(expResult, result);
    }

    /**
     * Test of dishType method, of class Meal.
     */
    @Test
    public void testDishType() {
        System.out.println("dishType");
        Calendar calendar = DateTime.parseDate("14-04-2017");

        DishType disht = new DishType("vege005", "vegetarian dish");
        Designation nome = Designation.valueOf("Peru");
        Money dinheiro = Money.euros(10);
        Dish dish = new Dish(disht, nome, dinheiro, new ArrayList<>());
        Meal instance = new Meal(calendar.getTime(), MealType.ALMOCO, dish);
        DishType expResult = new DishType("vege005", "vegetarian dish");
        DishType result = instance.dishType();
        assertEquals(expResult, result);
    }

    /**
     * Test of mealType method, of class Meal.
     */
    @Test
    public void testMealType() {
        System.out.println("mealType");
        MealType mealt = MealType.ALMOCO;
        Calendar calendar = DateTime.parseDate("14-04-2017");
        DishType disht = new DishType("Carne", "Carne");
        Money dinheiro = Money.euros(10);
        Designation nome = Designation.valueOf("Peru");
        Dish dish = new Dish(disht, nome, dinheiro, new ArrayList<>());
        Meal instance = new Meal(calendar.getTime(), MealType.ALMOCO, dish);
        MealType expResult = mealt;
        MealType result = instance.mealType();
        assertEquals(expResult, result);
    }

    /**
     * Test of dish method, of class Meal.
     */
    @Test
    public void testDish() {
        System.out.println("dish");
        MealType mealt = MealType.ALMOCO;
        Calendar calendar = DateTime.parseDate("14-04-2017");
        DishType disht = new DishType("Carne", "Carne");
        Money dinheiro = Money.euros(10);
        Designation nome = Designation.valueOf("Peru");
        Dish expResult = new Dish(disht, nome, dinheiro, new ArrayList<>());

        Meal instance = new Meal(calendar.getTime(), MealType.ALMOCO, expResult);

//   Dish expResult = new Dish(disht, nome, dinheiro);
        // Dish expResult = peru;
        Dish result = instance.dish();
        instance.dish();

        assertEquals(expResult, result);

    }

    /**
     * Test of equals method, of class Meal.
     */
    @Test
    public void testEquals() {
        System.out.println("equals");
        Calendar calendar = DateTime.parseDate("14-04-2017");
        DishType disht = new DishType("Carne", "Carne");
        Designation nome = Designation.valueOf("Peru");
        Money dinheiro = Money.euros(10);
        Dish dish = new Dish(disht, nome, dinheiro, new ArrayList<>());
        Meal instance = new Meal(calendar.getTime(), MealType.ALMOCO, dish);
        Object obj1 = new Meal(calendar.getTime(), MealType.ALMOCO, dish);
        Object obj2 = new Meal(calendar.getTime(), MealType.ALMOCO, dish);

        boolean expResult = false;
        boolean expResult2 = false;

        boolean result = instance.equals(obj1);
        boolean result2 = instance.equals(obj2);
        assertEquals(expResult, result);
    }

    /**
     * Test of indicarNumeroPratos method, of class Meal.
     */
    @Test
    public void testIndicarNumeroPratos() {
        System.out.println("indicarNumeroPratos");
        Calendar calendar = DateTime.parseDate("14-04-2017");
        DishType disht = new DishType("Carne", "Carne");
        Designation nome = Designation.valueOf("Peru");
        Money dinheiro = Money.euros(10);
        Dish dish = new Dish(disht, nome, dinheiro, new ArrayList<>());
        int numero = 2;
        Meal instance = new Meal(calendar.getTime(), MealType.ALMOCO, dish);
        instance.indicarNumeroPratos(numero);
    }

}
