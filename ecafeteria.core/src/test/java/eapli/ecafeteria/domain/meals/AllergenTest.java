/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.domain.meals;

import eapli.framework.domain.Designation;
import org.junit.Test;

/**
 *
 * @author <1131410 Fábio Ferreira>
 */
public class AllergenTest {

    private final Allergen a = new Allergen(Designation.valueOf("moluscos"));

    public AllergenTest() {
    }

    @Test(expected = IllegalStateException.class)
    public void testAllergenNameMustNotBeNull() {
        System.out.println("Deve conter um nome!");
        AllergenDish instance = new AllergenDish(null);
    }

}
