/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.domain.kitchen;

import eapli.ecafeteria.domain.meals.AllergenDish;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Pedro
 */
public class LoteTest {
    
    public LoteTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of materia_prima method, of class Lote.
     */
    @Test
    public void testMateria_prima() {
        System.out.println("materia_prima");
        Material mat = new Material("eggs","yo");
        Lote instance = new Lote("lote",mat);
        Material expResult = mat;
        Material result = instance.materia_prima();
        assertEquals(expResult, result);
    }

    /**
     * Test of is method, of class Lote.
     */
    @Test
    public void testIs() {
        System.out.println("is");
        String acronym = "lote";
        Material mat = new Material("eggs","yo");
        Lote instance = new Lote("lote",mat);
        boolean expResult = true;
        boolean result = instance.is(acronym);
        assertEquals(expResult, result);
    }

    /**
     * Test of equals method, of class Lote.
     */
    @Test
    public void testEquals() {
        System.out.println("equals");
        Material mat = new Material("eggs","yo");
        Lote instance = new Lote("lote",mat);
        Object o = (Object) instance;
        boolean expResult = true;
        boolean result = instance.equals(o);
        assertEquals(expResult, result);
    }

    /**
     * Test of id method, of class Lote.
     */
    @Test
    public void testId() {
        System.out.println("id");
        Material mat = new Material("eggs","yo");
        Lote instance = new Lote("lote",mat);
        String expResult = "lote";
        String result = instance.id();
        assertEquals(expResult, result);
    }
    
    @Test(expected = IllegalStateException.class)
    public void testLoteMustNotBeNull() {
        System.out.println("Nao deve ser null!");
        Lote instance = new Lote(null,null);
    }
    
}
