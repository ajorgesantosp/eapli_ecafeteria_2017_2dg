/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.domain.kitchen;

import eapli.framework.domain.ddd.AggregateRoot;
import eapli.framework.util.Strings;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Version;

/**
 *
 * @author Pedro
 */
@Entity
public class Lote implements AggregateRoot<String>, Serializable {
        
    private static final long serialVersionUID = 1L;

    // ORM primary key
    @Id
    @GeneratedValue
    private Long pk;
    @Version
    private Long version;

    // business ID
    @Column(unique = true)
    private String acronym;
    private Material materia_prima;

    protected Lote() {
        // for ORM
    }

    public Lote(String acronym, Material materia_prima) {
        if (Strings.isNullOrEmpty(acronym) || materia_prima == null) {
            throw new IllegalStateException();
        }
        this.acronym=acronym;
        this.materia_prima = materia_prima;
    }

    public Material materia_prima() {
        return this.materia_prima;
    }


    @Override
    public boolean is(String acronym) {
        return acronym.equalsIgnoreCase(this.acronym);
    }

    @Override
    public boolean sameAs(Object other) {
        // FIXME implement this method
        return false;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Lote)) {
            return false;
        }

        final Lote other = (Lote) o;
        return id().equals(other.id());
    }

    @Override
    public int hashCode() {
        return this.acronym.hashCode();
    }

    @Override
    public String id() {
        return this.acronym;
    }

    
}
