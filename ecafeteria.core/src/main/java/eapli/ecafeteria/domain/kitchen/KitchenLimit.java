/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.domain.kitchen;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 *
 * @author Flavio Relvas
 */
@Entity
public class KitchenLimit implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private float redLimit;
    private float yellowLimit;
    private boolean redAlert;
    private boolean yellowAlert;

    protected KitchenLimit() {
        //for ORM
    }

    public KitchenLimit(float yellowLimit, float redLimit) {
        this.yellowLimit = yellowLimit;
        this.redLimit = redLimit;
        this.redAlert = false;
        this.yellowAlert = false;
    }

    public void changeRedAlertStatus() {
        this.redAlert = !this.redAlert;
    }

    public void changeYellowAlertStatus() {
        this.yellowAlert = !this.yellowAlert;
    }

    public void resetAlertStatus() {
        this.redAlert = false;
        this.yellowAlert = false;
    }

    public float getRedLimit() {
        return redLimit;
    }

    public float getYellowLimit() {
        return yellowLimit;
    }

    public boolean isRedAlert() {
        return redAlert;
    }

    public boolean isYellowAlert() {
        return yellowAlert;
    }

    public void setRedLimit(float redLimit) {
        this.redLimit = redLimit;
    }

    public void setYellowLimit(float yellowLimit) {
        this.yellowLimit = yellowLimit;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof KitchenLimit)) {
            return false;
        }
        KitchenLimit other = (KitchenLimit) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "eapli.ecafeteria.domain.meals.KitchenLimits[ id=" + id + " ]";
    }

    public Long id() {
        return this.id;
    }

}
