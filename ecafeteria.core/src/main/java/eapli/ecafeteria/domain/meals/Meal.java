/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.domain.meals;

import eapli.ecafeteria.domain.kitchen.Lote;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
public class Meal implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long pk;

    @Temporal(TemporalType.DATE)
    private Date date;
    @ManyToOne
    private Dish dish;
    @Enumerated(EnumType.STRING)
    private MealType mealType;

    int numeroPratosCozinhados;
    List<Lote> lotesUtilizados;

    public Meal(final Date date, final MealType mealType, final Dish dish) {
        if (date == null || mealType == null || dish == null) {
            throw new IllegalStateException();
        }

        //if (validaData(date)==true) {
        this.date = date;
        // }

        this.mealType = mealType;
        this.dish = dish;
        this.numeroPratosCozinhados = -1;
        this.lotesUtilizados = new ArrayList<Lote>();
    }

    public Meal() {
        //ORM
    }

    public boolean validaData(Date teste) {
        Date dataact = new Date();
        if (teste.after(dataact)) {
            return true;
        } else {
            throw new IllegalArgumentException();
        }

    }

    public Date date() {
        return this.date;
    }

    public DishType dishType() {
        return this.dish.dishType();
    }

    public MealType mealType() {
        return this.mealType;
    }

    public Dish dish() {
        return this.dish;
    }

    public List<Lote> lotes() {
        return this.lotesUtilizados;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }

        final Meal other = (Meal) obj;
        return this.dish == other.dish && this.date == other.date;
    }

    public void indicarNumeroPratos(int numero) {
        this.numeroPratosCozinhados = numero;
    }

    @Override
    public String toString() {
        return String.format("Refeição: %s - %s - %s", this.dish.id().toString(), this.mealType.name(), this.date.toString());
    }

    public void indicarLotesUtilizados(List<Lote> lista) {
        this.lotesUtilizados = lista;
    }

}
