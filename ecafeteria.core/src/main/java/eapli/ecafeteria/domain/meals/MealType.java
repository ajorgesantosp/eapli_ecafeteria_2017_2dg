/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.domain.meals;

/**
 *
 * @author andre
 */
public enum MealType {
    ALMOCO,
    JANTAR;

    public static MealType[] mealTypes() {
        return new MealType[]{ALMOCO, JANTAR};
    }
}
