/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.domain.meals;

import eapli.framework.domain.Designation;
import eapli.framework.domain.ddd.AggregateRoot;
import java.io.Serializable;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Version;

/**
 *
 * @author Flavio Relvas
 */
@Entity
public class Allergen implements AggregateRoot<Designation>, Serializable {

    private static final long serialVersionUID = 1L;

    @Version
    private Long version;

    @EmbeddedId
    private Designation name;

    public Allergen(final Designation name) {
        if (name == null) {
            throw new IllegalStateException();
        }
        this.name = name;
    }

    protected Allergen() {
    }

    @Override
    public int hashCode() {
        return this.name.hashCode();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Dish)) {
            return false;
        }

        final Allergen other = (Allergen) o;
        return id().equals(other.id());
    }

    @Override
    public boolean sameAs(Object other) {
        if (!(other instanceof Allergen)) {
            return false;
        }

        final Allergen that = (Allergen) other;
        if (this == that) {
            return true;
        }

        return id().equals(that.id());
    }

    @Override
    public boolean is(Designation id) {
        return id().equals(id);
    }

    @Override
    public Designation id() {
        return this.name;
    }

    public Designation name() {
        return this.name;
    }

}
