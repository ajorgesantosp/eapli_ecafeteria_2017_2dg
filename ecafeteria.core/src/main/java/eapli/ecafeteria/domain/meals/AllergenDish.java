/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.domain.meals;

import eapli.framework.domain.ddd.ValueObject;
import java.io.Serializable;
import javax.persistence.Embeddable;

/**
 * a AllergenDish of a Dish.
 *
 *
 * TODO passwords should never be stored in plain format
 *
 * @author Flavio Relvas & Fábio Ferreira
 */
@Embeddable
public class AllergenDish implements ValueObject, Serializable {

    private static final long serialVersionUID = 1L;

    private String theAllergenDish;

    protected AllergenDish() {
        // for ORM only 
    }

    public AllergenDish(String allergen) {
        if (allergen == null) {
            throw new IllegalStateException();
        }
        this.theAllergenDish = allergen;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof AllergenDish)) {
            return false;
        }

        final AllergenDish theAllergenDish1 = (AllergenDish) o;

        return theAllergenDish1.equals(theAllergenDish1);

    }

    @Override
    public int hashCode() {
        return theAllergenDish.hashCode();
    }

    @Override
    public String toString() {
        return theAllergenDish;
    }

}
