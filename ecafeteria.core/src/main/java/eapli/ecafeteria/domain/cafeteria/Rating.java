/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.domain.cafeteria;

import eapli.ecafeteria.domain.meals.Meal;
import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

/**
 *
 * @author Andre
 */
@Entity
public class Rating implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    
    @ManyToOne
    private CafeteriaUser cafeteriaUser;
    @ManyToOne
    private Meal meal;
    private int rate;
    private String comment;

    public Rating(int rate, String comment, Meal meal, CafeteriaUser user) {
        if (meal == null || user == null) {
            throw new IllegalStateException();
        }
        if (rate < 1 || rate > 5) {
            throw new IllegalStateException();
        }
        if (comment.isEmpty()) {
            throw new IllegalStateException();
        }
        this.rate = rate;
        this.comment = comment;
        this.meal = meal;
        this.cafeteriaUser = user;
    }
    
    public Rating(){
        //ORM
    }
    
    public int rate(){
        return this.rate;
    }
    
    public String comment(){
        return this.comment;
    }
    
    public Meal meal(){
        return this.meal;
    }
    
    public CafeteriaUser user(){
        return this.cafeteriaUser;
    }
    
    public Long returnId() {
        return id;
    }
    
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Rating other = (Rating) obj;
        if (!Objects.equals(this.cafeteriaUser, other.cafeteriaUser)) {
            return false;
        }
        if (!Objects.equals(this.meal, other.meal)) {
            return false;
        }
        return true;
    }

}
