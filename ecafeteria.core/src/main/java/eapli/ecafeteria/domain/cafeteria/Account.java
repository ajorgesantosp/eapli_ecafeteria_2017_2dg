/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates and open the template
 * in the editor.
 */
package eapli.ecafeteria.domain.cafeteria;

import java.io.Serializable;
import javax.persistence.Embedded;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

/**
 *
 * @author 1120328
 */

@Entity
public class Account implements Serializable {

    @Id
    @GeneratedValue
    private Long id;

    @Embedded
    private final Balance balance;

    public Account() {
 
        this.balance = new Balance();  
    }



    public double saldo() {
        return this.balance.balance();
    }

    public boolean refreshBalance(double valor) {
        if (balance.validateCharging(valor)) {
            this.balance.refreshBalance(valor);
//          PersistenceContext.repositories().balance().save(this.m_saldo);
            return true;
        }
        return false;
    }

    public Balance balance() {
        return balance;
    }



    public Long getId() {
        return id;
    }



  
   
}
