/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.domain.cafeteria;

import eapli.framework.domain.ddd.ValueObject;
import java.io.Serializable;
import javax.persistence.Embeddable;

/**
 *
 * @author 1120328
 */
@Embeddable
public class Balance implements ValueObject, Serializable {

    private double balance;

    public Balance() {
        this.balance = 0.0;
    }

    public void refreshBalance(double valor) {

        this.balance += valor;

    }
    
    public void pagamento(double valor) {

        this.balance -= valor;

    }

    public boolean validaSaldo(double preco) {
        if (this.balance >= preco) {
            return true;
        } else {
            return false;
        }
    }

    public double balance() {
        return this.balance;
    }

    public boolean validateCharging(double v) {
        if (v > 0.0) {
            return true;
        } else {
            throw new IllegalArgumentException();
            
        }
    }
}
