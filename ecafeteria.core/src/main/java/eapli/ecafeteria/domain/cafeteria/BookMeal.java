/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.domain.cafeteria;

import eapli.ecafeteria.domain.meals.Meal;
import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

/**
 *
 * @author 1151109 Leonel Lima
 */
@Entity
public class BookMeal implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @ManyToOne
    private Meal MealSelected;
    @ManyToOne
    private CafeteriaUser user;

    @Enumerated(EnumType.STRING)
    private State state;

    public enum State {
        Reserved, Canceled, Delivered, NotDelivered, Rated
    }

    private boolean active;

    /**
     *
     * @param MealSelected
     * @param user
     */
    public BookMeal(Meal MealSelected, CafeteriaUser user) {
        if (MealSelected == null || user == null) {
            throw new IllegalStateException();
        }

        this.MealSelected = MealSelected;
        this.user = user;
        this.active = true;
    }

    protected BookMeal() {
        //ORM
    }

    public State state() {
        return this.state;
    }

    /**
     *
     * @return user
     */
    public CafeteriaUser user() {
        return this.user;
    }

    /**
     *
     * @return Meal Selected from user
     */
    public Meal MealSelected() {
        return this.MealSelected;
    }

    public void deliver() {
        if (this.state.equals(State.Reserved)) {
            this.state = State.Delivered;
        }
    }

    public void ratingstate() {
        if (this.state.equals(State.Delivered)) {
            this.state = State.Rated;
        }
    }
    public void reserva() {

        this.state = State.Reserved;

    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 89 * hash + Objects.hashCode(this.MealSelected);
        hash = 89 * hash + Objects.hashCode(this.user);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final BookMeal other = (BookMeal) obj;
        if (!Objects.equals(this.MealSelected, other.MealSelected)) {
            return false;
        }
        if (!Objects.equals(this.user, other.user)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "BookMeal{" + "MealSelected=" + MealSelected + ", user=" + user + ", active=" + active + '}';
    }
 
}
