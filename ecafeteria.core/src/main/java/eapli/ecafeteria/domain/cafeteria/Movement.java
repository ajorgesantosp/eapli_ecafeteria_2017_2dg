/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.domain.cafeteria;

import java.io.Serializable;
import java.util.Calendar;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;

/**
 *
 * @author 1120328
 */
@Entity
public class Movement implements Serializable {

    @Id
    @GeneratedValue
    private Long id;

    @OneToOne(cascade = CascadeType.MERGE)
    private Account acc;

    private double valor;

    @Temporal(javax.persistence.TemporalType.DATE)
    private Calendar day;

    public Movement() {
        this.day = Calendar.getInstance();
    }

    public Movement(Account acc, Double valor) {
        this.acc = acc;
        this.valor = valor;
        this.day = Calendar.getInstance();
    }

    public Long getId() {
        return this.id;
    }

    public Account getAcc() {
        return this.acc;
    }

    public boolean movCharge() {
        return acc.refreshBalance(valor);

    }

//    public boolean saveMov(Movement m) throws DataIntegrityViolationException{
//        return PersistenceContext.repositories().movements().save(new Movement(day));
//    }
}
