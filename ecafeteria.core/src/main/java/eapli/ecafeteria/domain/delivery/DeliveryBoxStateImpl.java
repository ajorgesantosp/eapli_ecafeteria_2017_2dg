/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.domain.delivery;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 *
 * @author Bernardo Dias
 */
@Entity
public abstract class DeliveryBoxStateImpl implements DeliveryBoxState, Serializable {

    @Id
    @GeneratedValue (strategy = GenerationType.IDENTITY)
    private Long id;
    
    private DeliveryBox deliveryBox;

    public DeliveryBoxStateImpl() {

    }

    public DeliveryBoxStateImpl(DeliveryBox deliveryBox) {
        this.deliveryBox = deliveryBox;
    }

    public DeliveryBox getDeliveryBox() {
        return this.deliveryBox;
    }

    @Override
    public boolean isInOpened() {
        return false;
    }

    @Override
    public boolean isInClosed() {
        return false;
    }

    @Override
    public boolean setStateOpened() {
        return false;
    }

    

    @Override
    public boolean setStateClosed() {
        return false;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
