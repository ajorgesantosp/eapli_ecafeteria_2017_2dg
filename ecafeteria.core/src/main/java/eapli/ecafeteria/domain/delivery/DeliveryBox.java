/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.domain.delivery;

import eapli.ecafeteria.domain.authz.SystemUser;
import eapli.ecafeteria.domain.meals.MealType;
import eapli.framework.domain.ddd.AggregateRoot;
import java.io.Serializable;
import java.util.Calendar;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;

/**
 *
 * @author Bernardo Dias
 */
@Entity
public class DeliveryBox implements AggregateRoot<Long>, Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    private DeliveryBoxStateImpl state;

    @Temporal(javax.persistence.TemporalType.DATE)
    private Calendar date;

    @Enumerated(EnumType.STRING)
    private MealType mealType;

    @ManyToOne(cascade = CascadeType.MERGE)
    private SystemUser user;

    public DeliveryBox() {
        state = new CreatedBoxState(this);
    }

    public DeliveryBox(Calendar date, MealType mealType, SystemUser user) {
        this.state = new CreatedBoxState(this);
        this.date = date;
        this.mealType = mealType;
        this.user = user;
    }

    public boolean setState(DeliveryBoxStateImpl state) {
        this.state = state;
        return true;
    }

    public Calendar date() {
        return this.date;
    }

    public SystemUser cashier() {
        return this.user;
    }

    public DeliveryBoxState boxState() {
        return this.state;
    }

    public MealType mealType() {
        return this.mealType;
    }

    @Override
    public boolean equals(Object other) {

        if (!(other instanceof DeliveryBox)) {
            return false;
        }
        final DeliveryBox that = (DeliveryBox) other;
        if (this == that) {
            return true;
        }
        if (!this.date.equals(that.date)) {
            return false;
        }

        if (!this.mealType.name().equals(that.mealType.name())) {
            return false;
        }
        
        return this.user.equals(that.user);

    }

    @Override
    public boolean sameAs(Object other) {
        return this.equals(other);
    }

    @Override
    public boolean is(Long id) {
        return this.id == id;
    }

    @Override
    public Long id() {
        return this.id;
    }
}
