/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.domain.delivery;

/**
 *
 * @author Bernardo Dias
 */
public interface DeliveryBoxState {
    
    boolean isInOpened();
    //boolean refeiçao a ser entregue implementem aqui();
    boolean isInClosed();
   
    boolean setStateOpened();
    //boolean refeiçao a ser entregue implementem aqui();
    boolean setStateClosed();
}
