/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.domain.delivery;

import javax.persistence.Entity;

/**
 *
 * @author Bernardo Dias
 */
@Entity
public class CreatedBoxState extends DeliveryBoxStateImpl {

    public CreatedBoxState() {
    }
    
    public CreatedBoxState(DeliveryBox deliveryBox) {
        super(deliveryBox);
    }
    
    @Override
    public boolean setStateOpened(){
        return getDeliveryBox().setState(new OpenedBoxState(getDeliveryBox()));
    }
}
