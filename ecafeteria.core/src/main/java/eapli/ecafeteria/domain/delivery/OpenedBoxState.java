/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.domain.delivery;

import javax.persistence.Entity;

/**
 *
 * @author Bernardo Dias
 */
@Entity
public class OpenedBoxState extends DeliveryBoxStateImpl {

    public OpenedBoxState() {
    }
    
    public OpenedBoxState(DeliveryBox deliveryBox) {
        super(deliveryBox);
    }

    public boolean isInOpened() {
        return true;
    }

    /*public boolean setStateClosed() {
        
    */}
