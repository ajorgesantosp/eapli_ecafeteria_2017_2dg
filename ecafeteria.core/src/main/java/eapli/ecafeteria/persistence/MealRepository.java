/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.persistence;

import eapli.ecafeteria.domain.meals.Meal;
import eapli.framework.persistence.repositories.DataRepository;
import java.util.Date;

/**
 *
 * @author andre
 */
public interface MealRepository extends DataRepository<Meal, Long>{
    Meal findByName (String name);
    Meal findByDate (Date d);
  
}
