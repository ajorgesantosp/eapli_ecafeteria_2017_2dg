/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.persistence;

import eapli.ecafeteria.domain.cafeteria.CafeteriaUser;
import eapli.ecafeteria.domain.cafeteria.BookMeal;
import eapli.ecafeteria.domain.cafeteria.MecanographicNumber;
import eapli.ecafeteria.domain.meals.DishType;
import eapli.ecafeteria.domain.meals.Meal;
import eapli.ecafeteria.domain.meals.MealType;
import eapli.framework.persistence.repositories.DataRepository;
import java.util.Calendar;
import java.util.Date;

/**
 *
 * @author Gaspa
 */
public interface BookMealRepository extends DataRepository<BookMeal, Long> {

    public Iterable<BookMeal> findByDateAndMealTypeAndDishType(Date date, MealType mealtype, DishType dishType);

    public Iterable<BookMeal> findByDate(Date sysDate, Date choiceDate);

    public Iterable<BookMeal> findReservedByUser(CafeteriaUser a);

    public Iterable<BookMeal> findBookingByUserAndDate(MecanographicNumber id, Date date);

    long countBookingsByMealAndDate(Meal meal, Date date);

    public Iterable<BookMeal> listBookedMealsNextNDays(CafeteriaUser cu, Calendar c);

    public Iterable<BookMeal> listBooketMealsbyday(CafeteriaUser cu, BookMeal c, Date ca);

    public boolean cancelReservation(BookMeal r);

    public Iterable<BookMeal> allBookedMealsByUserDelivery(CafeteriaUser user);

}
