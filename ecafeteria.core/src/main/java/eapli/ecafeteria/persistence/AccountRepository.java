/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.persistence;

import eapli.ecafeteria.domain.cafeteria.Account;
import eapli.framework.persistence.repositories.DataRepository;

/**
 *
 * @author 1120328
 */
public interface AccountRepository extends DataRepository<Account, Long> {
    
}
