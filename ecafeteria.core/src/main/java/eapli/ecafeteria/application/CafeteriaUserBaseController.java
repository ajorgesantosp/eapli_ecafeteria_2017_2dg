/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates and open the template
 * in the editor.
 */
package eapli.ecafeteria.application;

import eapli.ecafeteria.Application;
import eapli.ecafeteria.domain.authz.Username;
import eapli.ecafeteria.domain.cafeteria.CafeteriaUser;
import eapli.ecafeteria.persistence.CafeteriaUserRepository;
import eapli.ecafeteria.persistence.PersistenceContext;
import eapli.framework.application.Controller;
import eapli.framework.domain.Money;
import java.util.Currency;

/**
 *
 * @author mcn
 */
public class CafeteriaUserBaseController implements Controller {

    private final CafeteriaUserRepository repo = PersistenceContext.repositories().cafeteriaUsers(null);
    private CafeteriaUser cu;

    public Money balance() {
        Username username = Application.session().session().authenticatedUser().id();
        cu = repo.findByUsername(username);
        return new Money(cu.account().balance().balance(),Currency.getInstance("EUR"));
    }
}
