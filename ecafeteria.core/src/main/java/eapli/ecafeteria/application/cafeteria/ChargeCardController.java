/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.application.cafeteria;

import eapli.ecafeteria.domain.cafeteria.Account;
import eapli.ecafeteria.domain.cafeteria.CafeteriaUser;
import eapli.ecafeteria.domain.cafeteria.MecanographicNumber;
import eapli.ecafeteria.domain.cafeteria.Movement;
import eapli.ecafeteria.persistence.CafeteriaUserRepository;
import eapli.ecafeteria.persistence.MovementRepository;
import eapli.ecafeteria.persistence.PersistenceContext;
import eapli.framework.application.Controller;
import eapli.framework.persistence.DataConcurrencyException;
import eapli.framework.persistence.DataIntegrityViolationException;
import java.util.Iterator;

/**
 *
 * @author 1120328
 */
public class ChargeCardController implements Controller {

    private final MovementRepository movementRepository = PersistenceContext.repositories().movements();
    private final CafeteriaUserRepository cafUsersRepo = PersistenceContext.repositories().cafeteriaUsers(null); 

    public boolean chargeAccount(double saldo, Account account) throws DataIntegrityViolationException, DataConcurrencyException {  
        Movement m = new Movement(account, saldo);
        
        if (m.movCharge() == true) {
            movementRepository.save(m);
        }
        return false;
    }

    public Account getAccountID(MecanographicNumber number) {
        CafeteriaUser cafUser = cafUsersRepo.findByMecanographicNumber(number);

        if (cafUser != null) {
            return cafUser.account();
        }
        return null;
    }

}
