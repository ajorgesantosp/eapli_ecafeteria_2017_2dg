/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.application.cafeteria;

import eapli.ecafeteria.Application;
import eapli.ecafeteria.domain.authz.SystemUser;
import eapli.ecafeteria.domain.cafeteria.BookMeal;
import eapli.ecafeteria.domain.cafeteria.CafeteriaUser;

import eapli.ecafeteria.domain.meals.Meal;
import eapli.ecafeteria.persistence.BookMealRepository;
import eapli.ecafeteria.persistence.PersistenceContext;
import eapli.ecafeteria.persistence.RepositoryFactory;
import eapli.ecafeteria.persistence.jpa.JpaBookMealRepository;

import eapli.framework.application.Controller;
import eapli.framework.domain.Money;

import java.util.ArrayList;
import java.util.Currency;
import java.util.Date;
import java.util.List;
import java.util.Scanner;

/**
 *
 * @author Andre
 */
public class CancelReservationController implements Controller {

    BookMealRepository bo = new JpaBookMealRepository();
    CafeteriaUser user;
    Meal meal;
    Date date;
    RepositoryFactory repository = repositories();
    List<BookMeal> lstReservation;

    private RepositoryFactory repositories() {
        return PersistenceContext.repositories();
    }

    public boolean StartCancelReservation() {

        SystemUser a = Application.session().session().authenticatedUser();
        this.user = repository.cafeteriaUsers(null).findByUsername(a.username());
        Scanner scanIn = new Scanner(System.in);
        List<Meal> lstMeal = new ArrayList<>();
        lstReservation = new ArrayList<>();
        for (BookMeal res : bo.findAll()) {
            if (res.user().equals(this.user)) {
                lstMeal.add(res.MealSelected());
                lstReservation.add(res);
            }
        }
        if (lstMeal.isEmpty()) {
            throw new IllegalArgumentException("There are no Reservations");
        }
        System.out.println("Insert meal to cancel:\n");
        int i = 0;
        for (Meal meal1 : lstMeal) {
            System.out.printf("%d - %s\n", i, meal1.toString());
            i++;
        }
        int ans;
        ans = scanIn.nextInt();
        this.meal = lstMeal.get(ans);
        this.date = lstMeal.get(ans).date();
        cancelReservation(lstReservation.get(ans));
        Date x = new Date();
        if (date.after(x)) {
            this.user.account().balance().refreshBalance(meal.dish().currentPrice().amount());
        } else {
            if (meal.mealType() == meal.mealType().ALMOCO) {
                if (x.getHours() > 10) {
                    this.user.account().balance().refreshBalance(meal.dish().currentPrice().amount() / 2);
                }
                this.user.account().balance().refreshBalance(meal.dish().currentPrice().amount());
            }
            if (meal.mealType() == meal.mealType().JANTAR) {
                if (x.getHours() > 16) {
                    this.user.account().balance().refreshBalance(meal.dish().currentPrice().amount() / 2);
                }
                this.user.account().balance().refreshBalance(meal.dish().currentPrice().amount());
            }
        }
        return true;
    }

    public void setUser(CafeteriaUser User) {
        this.user = User;
    }

    public void meal(Meal meal) {
        this.meal = meal;
    }

    public void dateTime(Date date) {
        this.date = date;
    }

    public void cancelReservation(BookMeal r) {

        boolean cancel = PersistenceContext.repositories().Bookmeals().cancelReservation(r);
        //if (cancel) {
        //this.user.Account().actSaldo(1, date);
        //}

    }

}
