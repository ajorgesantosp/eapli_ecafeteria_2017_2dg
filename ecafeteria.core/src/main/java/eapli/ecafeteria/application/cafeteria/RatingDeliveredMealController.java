/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.application.cafeteria;

import eapli.ecafeteria.Application;
import eapli.ecafeteria.domain.authz.ActionRight;
import eapli.ecafeteria.domain.authz.Username;
import eapli.ecafeteria.domain.cafeteria.BookMeal;

import eapli.ecafeteria.domain.cafeteria.CafeteriaUser;
import eapli.ecafeteria.domain.cafeteria.Rating;

import eapli.ecafeteria.domain.meals.Meal;
import eapli.ecafeteria.persistence.BookMealRepository;

import eapli.ecafeteria.persistence.CafeteriaUserRepository;
import eapli.ecafeteria.persistence.PersistenceContext;
import eapli.ecafeteria.persistence.RatingRepository;

import eapli.framework.application.Controller;
import eapli.framework.persistence.DataConcurrencyException;
import eapli.framework.persistence.DataIntegrityViolationException;

/**
 *
 * @author PC
 */
public class RatingDeliveredMealController implements Controller {

    private final ListDeliveredMealsByUserService svc = new ListDeliveredMealsByUserService();

    private final RatingRepository ratingRepository = PersistenceContext.repositories().rating();
    private final CafeteriaUserRepository cafeteriaUserRepository = PersistenceContext.repositories().cafeteriaUsers(null);
    private final BookMealRepository bkmRepository = PersistenceContext.repositories().Bookmeals();
    CafeteriaUser user;

    public Rating saveRating(final int rate, final String comment,
            final Meal meal)
            throws DataIntegrityViolationException, DataConcurrencyException {
        {

            Application.ensurePermissionOfLoggedInUser(ActionRight.SELECT_MEAL);
            Username username = Application.session().session().authenticatedUser().id();
            user = cafeteriaUserRepository.findByUsername(username);

            final Rating newRating = new Rating(rate, comment, meal, user);

            Rating rat = this.ratingRepository.save(newRating);

            return rat;
        }
    }

    public void changeBookingMealStatus(BookMeal bkm)
            throws DataConcurrencyException, DataIntegrityViolationException {
        if (bkm == null) {
            throw new IllegalStateException();
        }
        bkm.ratingstate();
        this.bkmRepository.save(bkm);
    }

    public Iterable<BookMeal> deliveredMealsByUser() {
        Application.ensurePermissionOfLoggedInUser(ActionRight.SELECT_MEAL);

        Username username = Application.session().session().authenticatedUser().id();
        user = cafeteriaUserRepository.findByUsername(username);

        return this.svc.allBookedMealsByUserDelivery(user);
    }
}
