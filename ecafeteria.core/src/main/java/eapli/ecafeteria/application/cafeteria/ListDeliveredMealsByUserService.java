/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.application.cafeteria;

import eapli.ecafeteria.domain.cafeteria.BookMeal;
import eapli.ecafeteria.domain.cafeteria.CafeteriaUser;
import eapli.ecafeteria.persistence.BookMealRepository;
import eapli.ecafeteria.persistence.PersistenceContext;

/**
 *
 * @author andre
 */
public class ListDeliveredMealsByUserService {

    private final BookMealRepository bookedmealRepository = PersistenceContext.repositories().Bookmeals();

    public Iterable<BookMeal> allBookedMealsByUserDelivery(CafeteriaUser user) {
        return this.bookedmealRepository.allBookedMealsByUserDelivery(user);
    }
}
