
package eapli.ecafeteria.application.cafeteria;

import eapli.ecafeteria.Application;
import eapli.ecafeteria.domain.authz.ActionRight;
import eapli.ecafeteria.domain.cafeteria.BookMeal;

import eapli.ecafeteria.domain.cafeteria.CafeteriaUser;
import eapli.ecafeteria.persistence.BookMealRepository;

import eapli.ecafeteria.persistence.PersistenceContext;
import eapli.framework.application.Controller;
import java.util.Calendar;

/**
 *
 * @author Andre couto
 */
public class ListBookedMealsNextNDaysController implements Controller {
    
    private final BookMealRepository repository = PersistenceContext.repositories().Bookmeals();
    
    //list booked meals for the next n days
    //private 
    
    public Iterable<BookMeal> all() {
        Application.ensurePermissionOfLoggedInUser(ActionRight.SELECT_MEAL);
        return this.repository.findAll();
    }
    
    public Iterable<BookMeal> listBookedMealsNextNDays(CafeteriaUser cu,Calendar c){
        //Application.ensurePermissionOfLoggedInUser(ActionRight.);
        return this.repository.listBookedMealsNextNDays(cu, c);
    }
    
}
