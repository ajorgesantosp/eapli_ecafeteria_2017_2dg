/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.application.meals;

import eapli.ecafeteria.Application;
import eapli.ecafeteria.domain.authz.ActionRight;
import eapli.ecafeteria.domain.meals.Meal;
import eapli.ecafeteria.persistence.MealRepository;
import eapli.ecafeteria.persistence.PersistenceContext;

/**
 *
 * @author Pedro
 */
public class ListMealService {

   private MealRepository mealRepository = PersistenceContext.repositories().meals();

    public Iterable<Meal> allMeal() {
 
        Application.ensurePermissionOfLoggedInUser(ActionRight.MANAGE_KITCHEN);
        //Application.ensurePermissionOfLoggedInUser(ActionRight.SELECT_MEAL);
        return this.mealRepository.findAll();
    }
}

