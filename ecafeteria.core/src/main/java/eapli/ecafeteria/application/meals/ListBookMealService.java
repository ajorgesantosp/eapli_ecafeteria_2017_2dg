/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.application.meals;

import eapli.ecafeteria.Application;
import eapli.ecafeteria.domain.authz.ActionRight;
import eapli.ecafeteria.domain.cafeteria.CafeteriaUser;
import eapli.ecafeteria.domain.cafeteria.BookMeal;
import eapli.ecafeteria.persistence.BookMealRepository;
import eapli.ecafeteria.persistence.PersistenceContext;
import java.util.Calendar;
import java.util.Date;

/**
 *
 * @author léodoherty
 */
public class ListBookMealService {

    private BookMealRepository bookMealRepository = PersistenceContext.repositories().Bookmeals();

    public Iterable<BookMeal> allBookMeals() {
        Application.ensurePermissionOfLoggedInUser(ActionRight.MANAGE_MENUS);

        return this.bookMealRepository.findAll();
    }

    public Iterable<BookMeal> reservedBookMealsByUser(CafeteriaUser user) {
        Application.ensurePermissionOfLoggedInUser(ActionRight.SALE);
        Calendar today = Calendar.getInstance();
        
        return this.bookMealRepository.listBookedMealsNextNDays(user, today);
    }

}
