

package eapli.ecafeteria.application.meals;

import eapli.ecafeteria.Application;
import eapli.ecafeteria.domain.authz.ActionRight;
import eapli.ecafeteria.domain.meals.Meal;
import eapli.ecafeteria.persistence.MealRepository;
import eapli.ecafeteria.persistence.PersistenceContext;


public class ListMealServiceEcafeteria {

   private MealRepository mealRepository = PersistenceContext.repositories().meals();

    public Iterable<Meal> allMeal() {
 
     
        Application.ensurePermissionOfLoggedInUser(ActionRight.SELECT_MEAL);
        return this.mealRepository.findAll();
    }
} 

