/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.application.meals;

import eapli.ecafeteria.Application;
import eapli.ecafeteria.domain.authz.Username;
import eapli.ecafeteria.domain.cafeteria.CafeteriaUser;
import eapli.ecafeteria.domain.cafeteria.BookMeal;
import eapli.ecafeteria.domain.meals.Meal;
import eapli.ecafeteria.domain.meals.MealType;
import eapli.ecafeteria.persistence.AccountRepository;
import eapli.ecafeteria.persistence.BookMealRepository;
import eapli.ecafeteria.persistence.CafeteriaUserRepository;
import eapli.ecafeteria.persistence.PersistenceContext;
import eapli.framework.application.Controller;
import eapli.framework.persistence.DataConcurrencyException;
import eapli.framework.persistence.DataIntegrityViolationException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;

/**
 *
 * @author léodoherty
 */
public class RegisterBookMealController implements Controller {

    private BookMeal booking;

    private final BookMealRepository bookingRepository = PersistenceContext.repositories().Bookmeals();
    private final AccountRepository ac = PersistenceContext.repositories().accounts(null);
    // private final MealRepository mealRepository = PersistenceContext.repositories().meals();
    //  private final KitchenLimitRepository kitchenLimitRepository = PersistenceContext.repositories().kitchenLimits();
    // private final AllergenRepository allergenRepo = PersistenceContext.repositories().allergens();
    private final ListMealServiceEcafeteria lms = new ListMealServiceEcafeteria();
    // private Date data;
    private final CafeteriaUserRepository repo = PersistenceContext.repositories().cafeteriaUsers(null);
    private CafeteriaUser cu;

    /**
     *
     * @param data
     * @param mealType
     * @return
     */
    public ArrayList<Meal> getAvailableMeals(Date data, MealType mealType) {
        ArrayList<Meal> availableMeals = new ArrayList<>();
        availableMeals = listMeal(data, mealType);
        return availableMeals;
    }

    /**
     *
     * @param user
     * @param meal
     * @return
     */
    public boolean refreshSaldo(CafeteriaUser user, Meal meal) throws DataConcurrencyException, DataIntegrityViolationException {
        double preco = meal.dish().currentPrice().amount();
        //user.account().balance().refreshBalance(11);
        if (user.account().balance().validaSaldo(preco) == false) {
            System.out.println("Saldo insuficiente por favor carregue o cartao");
            return false;
        } else {
            System.out.println("Preço da refeiçao selecionada =" + preco);
        }
        user.account().balance().validaSaldo(preco);
        user.account().balance().pagamento(preco);

        ac.save(user.account());
        System.out.println("Saldo atual " + user.account().balance().balance());

        return true;
    }

    // não existe ementa por isso nao funciona
    /**
     *
     * @param user
     * @param meal
     * @return
     */
    public boolean findAlergen(CafeteriaUser user, Meal meal) {

//        if(user.organicUnit().equals(meal.dish().allergens())){
//            return true;
//        }
        return false;
    }

//    /**
//     *
//     * @param user
//     * @param meal
//     * @throws DataConcurrencyException
//     * @throws DataIntegrityViolationException
//     */
//    public void createBook(CafeteriaUser user, Meal meal) throws DataConcurrencyException, DataIntegrityViolationException {
//
//        booking = new BookMeal(meal, user);
//
//        bookingRepository.save(booking);
//        new GenerateAlertsController().generateKitchenAlerts(meal, data);
//    }
    /**
     *
     * @param b
     * @throws DataConcurrencyException
     * @throws DataIntegrityViolationException
     */
    public void createBooking(BookMeal b) throws DataConcurrencyException, DataIntegrityViolationException {

        bookingRepository.save(b);
    }

    public boolean booking(Meal meal) throws DataConcurrencyException, DataIntegrityViolationException {
        Username username = Application.session().session().authenticatedUser().id();
        cu = repo.findByUsername(username);
        booking = new BookMeal(meal, cu);
        booking.reserva();
        //long it = bookingRepository.countBookingsByMealAndDate(booking.MealSelected(),booking.MealSelected().date());
        
        //if(it==0){
        refreshSaldo(cu, meal);
        bookingRepository.save(booking);
        //}
        //System.out.println("Ja tem reserva efectuada");
        return true;
    }

    /*
    Método para testes
     */
    /**
     *
     * @param data
     * @param mealType
     * @return
     */
    public ArrayList<Meal> listMeal(Date data, MealType mealType) {
        //   ArrayList<Meal> meals = new ArrayList<>();
        ArrayList<Meal> meallist = new ArrayList<>();

        // Meal meal = new Meal();
        // Meal meal = new Meal(new Date(12, 05, 2017), MealType.ALMOCO, new Dish(new DishType("qwe", "qwe"),"", 5, "qwe"));
        Iterable<Meal> meals1 = meals();
        for (Meal meal1 : meals1) {

            if (meal1.mealType().equals(mealType)) {
                if (meal1.date().equals(data)) {
                    meallist.add(meal1);

                }
            }

        }

        //DishType dishType1 = new DishType("Peixe", "Bacalhau");
//        Dish dish1 = new Dish(dishType1, Designation.valueOf("Bacalhau com natas"), new Money(23, Currency.getInstance("USD")));
//           Meal meal1 = new Meal((long)2, MealType.ALMOCO, dish1, new Date(1, 1, 2001));
//
//        Meal meal1 = new Meal(new Date(1999, 12, 5), mealType, dish1);
//
//        if (meal1.date().equals(data) && meal1.mealType().equals(mealType)) {
//            meals.add(meal1);
//        }
        return meallist;
    }

    public Iterable<Meal> meals() {
        return this.lms.allMeal();
    }

}
