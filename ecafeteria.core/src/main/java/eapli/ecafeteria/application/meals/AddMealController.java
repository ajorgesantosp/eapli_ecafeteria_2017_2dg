/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.application.meals;

import eapli.ecafeteria.domain.meals.Dish;
import eapli.ecafeteria.domain.meals.Meal;
import eapli.ecafeteria.domain.meals.MealType;
import eapli.ecafeteria.persistence.MealRepository;
import eapli.ecafeteria.persistence.PersistenceContext;
import eapli.framework.application.Controller;
import eapli.framework.persistence.DataConcurrencyException;
import eapli.framework.persistence.DataIntegrityViolationException;
import java.util.Calendar;
import java.util.Date;

/**
 *
 * @author andre
 */
public class AddMealController implements Controller{

    private ListDishService svc = new ListDishService();

    private MealRepository menuRepository = PersistenceContext.repositories().meals();
    
    public Meal createMeal(final Date date, final MealType mealType, Dish dish)
            throws DataIntegrityViolationException, DataConcurrencyException {

        final Meal newMeal = new Meal(date, mealType, dish);

        Meal ret = this.menuRepository.save(newMeal);
        return ret;
    }

    public Iterable<Dish> dishes() {
        return this.svc.allDishes();
    }

    public MealType getMealType(String mealType) {
        if (mealType.equalsIgnoreCase("AlMOCO")) {
            return MealType.ALMOCO;
        } else {
            return MealType.JANTAR;
        }
    }
}
