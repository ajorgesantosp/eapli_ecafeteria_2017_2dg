/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.application.meals;

import eapli.ecafeteria.Application;
import eapli.ecafeteria.domain.authz.ActionRight;
import eapli.ecafeteria.domain.kitchen.Lote;
import eapli.ecafeteria.persistence.LoteRepository;
import eapli.ecafeteria.persistence.PersistenceContext;

/**
 *
 * @author Pedro
 */
public class ListLotesService {
    private LoteRepository loteRepository = PersistenceContext.repositories().lotes();

    public Iterable<Lote> allLotes() {
        Application.ensurePermissionOfLoggedInUser(ActionRight.MANAGE_KITCHEN);

        return this.loteRepository.findAll();
    }
    
}
