/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.application.meals;

import eapli.ecafeteria.Application;
import eapli.ecafeteria.domain.authz.ActionRight;
import eapli.ecafeteria.domain.cafeteria.CafeteriaUser;
import eapli.ecafeteria.domain.cafeteria.MecanographicNumber;
import eapli.ecafeteria.domain.cafeteria.BookMeal;
import eapli.ecafeteria.persistence.BookMealRepository;
import eapli.ecafeteria.persistence.CafeteriaUserRepository;
import eapli.ecafeteria.persistence.PersistenceContext;
import eapli.framework.application.Controller;
import eapli.framework.persistence.DataConcurrencyException;
import eapli.framework.persistence.DataIntegrityViolationException;

/**
 *
 * @author Pedrobarbosa
 */
public class CheckInReservationController implements Controller {

    private final ListBookMealService svc = new ListBookMealService();

    private final CafeteriaUserRepository userRepo = PersistenceContext.repositories().cafeteriaUsers(null);
    private final BookMealRepository bookMealRepo = PersistenceContext.repositories().Bookmeals();

    public BookMeal deliver(BookMeal theBookMeal) throws DataConcurrencyException, DataIntegrityViolationException {
        Application.ensurePermissionOfLoggedInUser(ActionRight.SALE);

        if (theBookMeal == null) {
            throw new IllegalStateException();
        }

        theBookMeal.deliver();

        return this.bookMealRepo.save(theBookMeal);

    }

    public Iterable<BookMeal> listBookMeals(CafeteriaUser user) {
        return this.svc.reservedBookMealsByUser(user);
    }

    public CafeteriaUser getAccountID(MecanographicNumber number) {
        return this.userRepo.findByMecanographicNumber(number);
    }

}
