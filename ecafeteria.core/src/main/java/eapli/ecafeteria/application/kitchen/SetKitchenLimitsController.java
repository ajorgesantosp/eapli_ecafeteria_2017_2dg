/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.application.kitchen;

import eapli.ecafeteria.Application;
import eapli.ecafeteria.domain.authz.ActionRight;
import eapli.ecafeteria.domain.kitchen.KitchenLimit;
import eapli.ecafeteria.persistence.KitchenLimitRepository;
import eapli.ecafeteria.persistence.PersistenceContext;
import eapli.framework.application.Controller;
import eapli.framework.persistence.DataConcurrencyException;
import eapli.framework.persistence.DataIntegrityViolationException;

/**
 *
 * @author <1131410 Fábio Ferreira>
 */
public class SetKitchenLimitsController implements Controller {

    private final KitchenLimitRepository limitRepository = PersistenceContext.repositories().kitchenLimits();

    public KitchenLimit updateKitchenLimit(float yellowLimit, float redLimit)
            throws DataIntegrityViolationException, DataConcurrencyException {
        Application.ensurePermissionOfLoggedInUser(ActionRight.ADMINISTER);

        final KitchenLimit kitLimit = this.limitRepository.first();
        kitLimit.setYellowLimit(yellowLimit);
        kitLimit.setRedLimit(redLimit);
        return this.limitRepository.save(kitLimit);
    }
}
