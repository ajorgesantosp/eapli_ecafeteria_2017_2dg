/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.application.kitchen;

import eapli.ecafeteria.Application;
import eapli.ecafeteria.application.meals.ListMealService;
import eapli.ecafeteria.domain.authz.ActionRight;
import eapli.ecafeteria.domain.meals.Meal;
import eapli.ecafeteria.persistence.MealRepository;
import eapli.ecafeteria.persistence.PersistenceContext;
import eapli.framework.application.Controller;
import eapli.framework.persistence.DataConcurrencyException;
import eapli.framework.persistence.DataIntegrityViolationException;

/**
 *
 * @author Pedro
 */
public class NumberOfDishesController implements Controller{
    
    private ListMealService svc = new ListMealService();
    private final MealRepository mealRepository = PersistenceContext.repositories().meals();
    
   public void indicarNumeroPratos(Meal meal, int numero) throws DataConcurrencyException, DataIntegrityViolationException {
        Application.ensurePermissionOfLoggedInUser(ActionRight.MANAGE_KITCHEN);
        if (meal == null) {
            throw new IllegalArgumentException();
        }
        meal.indicarNumeroPratos(numero);

        Meal ret = this.mealRepository.save(meal);
    }

    public Iterable<Meal> meals() {
        return this.svc.allMeal();
    }
    
}

