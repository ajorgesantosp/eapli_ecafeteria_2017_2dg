/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.application.kitchen;

import eapli.ecafeteria.Application;
import eapli.ecafeteria.domain.authz.ActionRight;
import eapli.ecafeteria.domain.kitchen.Lote;
import eapli.ecafeteria.domain.kitchen.Material;
import eapli.ecafeteria.persistence.LoteRepository;
import eapli.ecafeteria.persistence.MaterialRepository;
import eapli.ecafeteria.persistence.PersistenceContext;
import eapli.framework.application.Controller;
import eapli.framework.persistence.DataConcurrencyException;
import eapli.framework.persistence.DataIntegrityViolationException;

/**
 *
 * @author Pedro
 */
public class RegisterLoteController implements Controller {
    private final LoteRepository repository = PersistenceContext.repositories().lotes();

    public Lote registerLote(String id, Material materia_prima)
            throws DataIntegrityViolationException, DataConcurrencyException {
        Application.ensurePermissionOfLoggedInUser(ActionRight.MANAGE_KITCHEN);

        final Lote lote = new Lote(id, materia_prima);
        return this.repository.save(lote);
    }
}
