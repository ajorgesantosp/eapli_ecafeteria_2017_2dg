/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.application.kitchen;

import eapli.ecafeteria.domain.kitchen.KitchenLimit;
import eapli.ecafeteria.domain.meals.Meal;
import eapli.ecafeteria.persistence.BookMealRepository;
import eapli.ecafeteria.persistence.KitchenLimitRepository;
import eapli.ecafeteria.persistence.PersistenceContext;
import eapli.framework.application.Controller;
import eapli.framework.persistence.DataConcurrencyException;
import eapli.framework.persistence.DataIntegrityViolationException;
import java.util.Date;

/**
 *
 * @author Flavio Relvas
 */
public class GenerateAlertsController implements Controller {

    private final BookMealRepository bookingRepository = PersistenceContext.repositories().Bookmeals();
    private final KitchenLimitRepository limitRepository = PersistenceContext.repositories().kitchenLimits();
    //@TODO - add menu repository
    
    public void generateKitchenAlerts(Meal meal, Date date) throws DataConcurrencyException, DataIntegrityViolationException{
        int bookedMeals = Integer.parseInt(Long.toString(bookingRepository.countBookingsByMealAndDate(meal, date)));
        int plannedMeals = 1; // = menuRepository.getNumberofDishesByMealAndDate(meal, date);
        KitchenLimit kitchenLimit = limitRepository.first();
        
        if( bookedMeals/plannedMeals >= kitchenLimit.getRedLimit()){
            kitchenLimit.resetAlertStatus();
            kitchenLimit.changeRedAlertStatus();
        } else if(bookedMeals/plannedMeals >= kitchenLimit.getYellowLimit()){
            kitchenLimit.resetAlertStatus();
            kitchenLimit.changeYellowAlertStatus();
        }
        
        limitRepository.save(kitchenLimit);
    }
}
