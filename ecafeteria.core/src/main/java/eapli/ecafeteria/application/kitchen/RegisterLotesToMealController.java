/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.application.kitchen;

import eapli.ecafeteria.Application;
import eapli.ecafeteria.application.meals.ListLotesService;
import eapli.ecafeteria.application.meals.ListMealService;
import eapli.ecafeteria.domain.authz.ActionRight;
import eapli.ecafeteria.domain.kitchen.Lote;
import eapli.ecafeteria.domain.meals.Meal;
import eapli.ecafeteria.persistence.MealRepository;
import eapli.ecafeteria.persistence.PersistenceContext;
import eapli.framework.application.Controller;
import eapli.framework.persistence.DataConcurrencyException;
import eapli.framework.persistence.DataIntegrityViolationException;
import java.util.List;

/**
 *
 * @author Pedro
 */
public class RegisterLotesToMealController implements Controller {
    private ListMealService svc = new ListMealService();
    private ListLotesService lvc = new ListLotesService();
    private final MealRepository mealRepository = PersistenceContext.repositories().meals();
    
    public void indicarLotesUtilizados(Meal meal, List<Lote> lista) throws DataConcurrencyException, DataIntegrityViolationException {
        Application.ensurePermissionOfLoggedInUser(ActionRight.MANAGE_KITCHEN);
        if (meal == null) {
            throw new IllegalArgumentException();
        }
        meal.indicarLotesUtilizados(lista);

        Meal ret = this.mealRepository.save(meal);
    }
    
    public Iterable<Meal> meals() {
        return this.svc.allMeal();
    }
    
    public Iterable<Lote> lotes() {
        return this.lvc.allLotes();
    }
}
