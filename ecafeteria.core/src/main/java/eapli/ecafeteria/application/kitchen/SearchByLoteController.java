/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.application.kitchen;

import eapli.ecafeteria.Application;
import eapli.ecafeteria.application.meals.ListLotesService;
import eapli.ecafeteria.application.meals.ListMealService;
import eapli.ecafeteria.domain.authz.ActionRight;
import eapli.ecafeteria.domain.kitchen.Lote;
import eapli.ecafeteria.domain.meals.Meal;
import eapli.ecafeteria.persistence.MealRepository;
import eapli.ecafeteria.persistence.PersistenceContext;
import eapli.framework.application.Controller;
import eapli.framework.persistence.DataConcurrencyException;
import eapli.framework.persistence.DataIntegrityViolationException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Pedro
 */
public class SearchByLoteController implements Controller {
    private ListMealService svc = new ListMealService();
    private ListLotesService lvc = new ListLotesService();
    
    public List<Meal> pesquisarPorLote(Lote lote) throws DataConcurrencyException, DataIntegrityViolationException {
        Application.ensurePermissionOfLoggedInUser(ActionRight.MANAGE_KITCHEN);
        if (lote == null) {
            throw new IllegalArgumentException();
        }
        Iterable <Meal> meals = meals();
        List <Meal> lista_meals_lote=new ArrayList();
        
        for( Meal m : meals){
            List<Lote> lotes = m.lotes();
            for(Lote l : lotes){
                if(l.is(lote.id())){
                    lista_meals_lote.add(m);
                }
            }
        }
        
        return lista_meals_lote;
        
    }
    
    public Iterable<Meal> meals() {
        return this.svc.allMeal();
    }
    
    public Iterable<Lote> lotes() {
        return this.lvc.allLotes();
    }
}
