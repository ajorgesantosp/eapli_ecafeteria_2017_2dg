/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.bootstrapers;

import eapli.ecafeteria.application.meals.AddMealController;
import eapli.ecafeteria.application.meals.RegisterDishController;
import eapli.ecafeteria.domain.meals.Dish;
import eapli.ecafeteria.domain.meals.DishType;
import eapli.ecafeteria.domain.meals.MealType;
import eapli.ecafeteria.persistence.DishRepository;
import eapli.ecafeteria.persistence.DishTypeRepository;
import eapli.ecafeteria.persistence.PersistenceContext;
import eapli.framework.actions.Action;
import eapli.framework.domain.Designation;
import eapli.framework.persistence.DataConcurrencyException;
import eapli.framework.persistence.DataIntegrityViolationException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.logging.Logger;

/**
 *
 * @author Pedro
 */
public class MealBootstrapper implements Action {

    @Override
    public boolean execute() {

        final DishRepository dishRepo = PersistenceContext.repositories().dishes();
        final Iterable<Dish> um = dishRepo.findAll();
        Calendar today = Calendar.getInstance();
    
       
        register(today.getTime(), MealType.ALMOCO, um.iterator().next());
        register(today.getTime(), MealType.JANTAR, um.iterator().next());

        return false;
    }

    /**
     *
     */
    private void register(Date data, MealType mealType, Dish prato) {
        final AddMealController controller = new AddMealController();
        try {
            controller.createMeal(data, mealType, prato);
        } catch (final DataIntegrityViolationException | DataConcurrencyException e) {
            // ignoring exception. assuming it is just a primary key violation
            // due to the tentative of inserting a duplicated user
            Logger.getLogger(ECafeteriaBootstraper.class.getSimpleName())
                    .info("EAPLI-DI001: bootstrapping existing record");
        }
    }
}
