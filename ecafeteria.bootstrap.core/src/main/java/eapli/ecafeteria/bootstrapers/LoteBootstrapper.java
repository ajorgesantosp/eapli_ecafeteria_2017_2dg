/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.bootstrapers;

import eapli.ecafeteria.application.kitchen.RegisterLoteController;
import eapli.ecafeteria.application.kitchen.RegisterMaterialController;
import eapli.ecafeteria.domain.kitchen.Material;
import eapli.ecafeteria.persistence.DishRepository;
import eapli.ecafeteria.persistence.MaterialRepository;
import eapli.ecafeteria.persistence.PersistenceContext;
import eapli.framework.actions.Action;
import eapli.framework.persistence.DataConcurrencyException;
import eapli.framework.persistence.DataIntegrityViolationException;
import java.util.logging.Logger;

/**
 *
 * @author Pedro
 */
public class LoteBootstrapper implements Action {

    @Override
    public boolean execute() {
        final MaterialRepository matRepo = PersistenceContext.repositories().materials();
        Material mat1 = matRepo.findAll().iterator().next();
        Material mat2 = matRepo.findAll().iterator().next();
        Material mat3=matRepo.findByAcronym("oil");
        
        register("lot1", mat1);
        register("lot2", mat2);
        register("lot3",mat3);

        return false;
    }
    
    private void register(String id, Material mat) {
        final RegisterLoteController controller = new RegisterLoteController();
        try {
            controller.registerLote(id, mat);
        } catch (final DataIntegrityViolationException | DataConcurrencyException e) {
            // ignoring exception. assuming it is just a primary key violation
            // due to the tentative of inserting a duplicated user
            Logger.getLogger(ECafeteriaBootstraper.class.getSimpleName())
                    .info("EAPLI-DI001: bootstrapping existing record");
        }
    }

}
