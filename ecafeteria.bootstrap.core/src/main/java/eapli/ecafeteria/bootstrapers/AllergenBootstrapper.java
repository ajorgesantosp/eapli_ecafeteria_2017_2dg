/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.bootstrapers;

import eapli.ecafeteria.domain.meals.Allergen;
import eapli.ecafeteria.persistence.AllergenRepository;
import eapli.ecafeteria.persistence.PersistenceContext;
import eapli.framework.actions.Action;
import eapli.framework.domain.Designation;
import eapli.framework.persistence.DataConcurrencyException;
import eapli.framework.persistence.DataIntegrityViolationException;
import java.util.logging.Logger;

/**
 *
 * @author Flavio Relvas
 */
public class AllergenBootstrapper implements Action{

    @Override
    public boolean execute() {
        register("gluten");
        register("crustaceos");
        register("ovos");
        register("peixe");
        register("amendoins");
        register("soja");
        register("lactose");
        register("frutos casca rija");
        register("aipo");
        register("mostarda");
        register("sementes de sesamo");
        register("enxofre");
        register("tremoco");
        register("moluscos");
        return false;
    }
    
    private void register(String name){
        final AllergenRepository allergenRepo = PersistenceContext.repositories().allergens();
        try{
            allergenRepo.save(new Allergen(Designation.valueOf(name)));
        }catch (final DataIntegrityViolationException | DataConcurrencyException e) {
            // ignoring exception. assuming it is just a primary key violation
            // due to the tentative of inserting a duplicated user
            Logger.getLogger(ECafeteriaBootstraper.class.getSimpleName())
                    .info("EAPLI-DI001: bootstrapping existing record");
        }
    }
}
