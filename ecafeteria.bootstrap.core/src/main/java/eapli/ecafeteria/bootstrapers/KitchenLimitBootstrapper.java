/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.bootstrapers;

import eapli.ecafeteria.domain.kitchen.KitchenLimit;
import eapli.ecafeteria.persistence.KitchenLimitRepository;
import eapli.ecafeteria.persistence.PersistenceContext;
import eapli.framework.actions.Action;
import eapli.framework.persistence.DataConcurrencyException;
import eapli.framework.persistence.DataIntegrityViolationException;
import java.util.logging.Logger;

/**
 *
 * @author Flavio Relvas
 */
public class KitchenLimitBootstrapper implements Action {

    @Override
    public boolean execute() {
        register();
        return false;
    }
    
    private void register(){
        final KitchenLimitRepository limitsRepo = PersistenceContext.repositories().kitchenLimits();
        try{
            limitsRepo.save(new KitchenLimit(0.75f, 0.90f));
        }catch (final DataIntegrityViolationException | DataConcurrencyException e) {
            // ignoring exception. assuming it is just a primary key violation
            // due to the tentative of inserting a duplicated user
            Logger.getLogger(ECafeteriaBootstraper.class.getSimpleName())
                    .info("EAPLI-DI001: bootstrapping existing record");
        }
    }

}
