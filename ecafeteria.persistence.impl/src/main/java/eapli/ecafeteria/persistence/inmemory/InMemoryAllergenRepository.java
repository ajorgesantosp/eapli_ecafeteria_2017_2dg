/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.persistence.inmemory;

import eapli.ecafeteria.domain.meals.Allergen;
import eapli.ecafeteria.persistence.AllergenRepository;
import eapli.framework.domain.Designation;
import eapli.framework.persistence.repositories.impl.inmemory.InMemoryRepository;

/**
 *
 * @author Flavio Relvas
 */
public class InMemoryAllergenRepository extends InMemoryRepository<Allergen, Designation> implements AllergenRepository {

    @Override
    protected Designation newPK(Allergen entity) {
        return entity.id();
    }

    @Override
    public Allergen findByName(Designation name) {
        return matchOne(e -> e.name().equals(name));
    }

}
