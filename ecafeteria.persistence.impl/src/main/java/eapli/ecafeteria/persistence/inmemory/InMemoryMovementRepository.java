/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.persistence.inmemory;

import eapli.ecafeteria.domain.cafeteria.Movement;
import eapli.ecafeteria.persistence.MovementRepository;
import eapli.framework.persistence.repositories.impl.inmemory.InMemoryRepositoryWithLongPK;

/**
 *
 * @author 1120328
 */
public class InMemoryMovementRepository extends InMemoryRepositoryWithLongPK<Movement> implements MovementRepository {

    public Movement findById(Long id) {
        return matchOne(e -> e.getId().equals(id));
    }
    
}
