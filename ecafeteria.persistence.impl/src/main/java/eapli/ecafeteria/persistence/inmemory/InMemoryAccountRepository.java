/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.persistence.inmemory;

import eapli.ecafeteria.domain.cafeteria.Account;
import eapli.ecafeteria.persistence.AccountRepository;
import eapli.framework.persistence.repositories.impl.inmemory.InMemoryRepositoryWithLongPK;

/**
 *
 * @author 1120328
 */
public class InMemoryAccountRepository extends InMemoryRepositoryWithLongPK<Account> implements AccountRepository{
    
}
