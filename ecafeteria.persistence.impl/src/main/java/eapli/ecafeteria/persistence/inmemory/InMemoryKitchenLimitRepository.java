/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.persistence.inmemory;

import eapli.ecafeteria.domain.kitchen.KitchenLimit;
import eapli.ecafeteria.persistence.KitchenLimitRepository;
import eapli.framework.persistence.repositories.impl.inmemory.InMemoryRepository;

/**
 *
 * @author Flavio Relvas
 */
public class InMemoryKitchenLimitRepository extends InMemoryRepository<KitchenLimit, Long>implements KitchenLimitRepository{

    @Override
    protected Long newPK(KitchenLimit entity) {
        return entity.id();
    }
    
}
