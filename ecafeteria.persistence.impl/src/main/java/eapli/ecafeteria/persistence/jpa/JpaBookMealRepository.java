/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.persistence.jpa;

import eapli.ecafeteria.Application;
import eapli.ecafeteria.domain.cafeteria.BookMeal;
import eapli.ecafeteria.domain.cafeteria.CafeteriaUser;
import eapli.ecafeteria.domain.cafeteria.MecanographicNumber;
import eapli.ecafeteria.domain.meals.DishType;
import eapli.ecafeteria.domain.meals.Meal;
import eapli.ecafeteria.domain.meals.MealType;
import eapli.ecafeteria.persistence.BookMealRepository;
import eapli.framework.persistence.repositories.TransactionalContext;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import javax.persistence.Query;
import javax.persistence.TemporalType;

/**
 *
 * @author léodoherty
 */
public class JpaBookMealRepository extends CafeteriaJpaRepositoryBase<BookMeal, Long> implements BookMealRepository {

//    public JpaBookMealRepository(TransactionalContext autoTx) {
//        super(Application.settings().getPersistenceUnitName());
//    }
    @Override
    public long countBookingsByMealAndDate(Meal meal, Date date) {
        return countWhere("e.meal=:" + meal, "e.date=:" + date);
    }

    @Override
    public Iterable<BookMeal> findByDateAndMealTypeAndDishType(Date date, MealType mealtype, DishType dishType) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Iterable<BookMeal> findByDate(Date sysDate, Date choiceDate) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Iterable<BookMeal> findReservedByUser(CafeteriaUser a) {
        return matchWithTwoParams("e.user=:" + a.id(), "e.state=:" + BookMeal.State.Reserved);
    }

    @Override
    public Iterable<BookMeal> findBookingByUserAndDate(MecanographicNumber id, Date date) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Iterable<BookMeal> listBookedMealsNextNDays(CafeteriaUser cu, Calendar c) {

        final Query q = entityManager().createQuery(""
                + "SELECT e FROM BookMeal e "
                + "WHERE e.user =?1 "
                + " AND e.state =?2 "
                + " AND e.MealSelected=(SELECT M FROM Meal M WHERE M.date=?3)");
        q.setParameter(1, cu);
        q.setParameter(2, BookMeal.State.Reserved);
        q.setParameter(3, c, TemporalType.DATE);
        return q.getResultList();
    }

    @Override
    public Iterable<BookMeal> listBooketMealsbyday(CafeteriaUser cu, BookMeal c, Date ca) {
        final Query q = entityManager().createQuery(""
                + "SELECT e FROM BookMeal e "
                + "WHERE e.user =?1 "
                + " AND e.state =?2 "
                + " AND e.MEALSELECTED_PK=(SELECT M FROM Meal M WHERE M.date=?3)");
        q.setParameter(1, cu.id());
        q.setParameter(2, BookMeal.State.Reserved);
        q.setParameter(3, ca, TemporalType.DATE);
        return q.getResultList();
    }

    @Override
    public boolean cancelReservation(BookMeal r) {
        super.entityManager().getTransaction().begin();
        BookMeal aux = super.entityManager().merge(r);
        super.entityManager().remove(aux);
        super.entityManager().getTransaction().commit();
        return true;
    }

    @Override
    public Iterable<BookMeal> allBookedMealsByUserDelivery(CafeteriaUser user) {
        final Query q = entityManager().createQuery(""
                + "SELECT e FROM BookMeal e "
                + "WHERE e.user =?1 "
                + "AND e.state =?2 ");
        q.setParameter(1, user);
        q.setParameter(2, BookMeal.State.Delivered);
     
        return q.getResultList();
    }

}
