/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.persistence.jpa;

import eapli.ecafeteria.domain.kitchen.Lote;
import eapli.ecafeteria.persistence.LoteRepository;

/**
 *
 * @author Pedro
 */
public class JpaLoteRepository extends CafeteriaJpaRepositoryBase<Lote, Long> implements LoteRepository {
    @Override
    public Lote findById(String id) {
        return matchOne("e.id=:id", "id", id);
    }
}