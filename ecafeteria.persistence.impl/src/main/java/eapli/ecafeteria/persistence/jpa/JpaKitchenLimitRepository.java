/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.persistence.jpa;

import eapli.ecafeteria.domain.kitchen.KitchenLimit;
import eapli.ecafeteria.persistence.KitchenLimitRepository;

/**
 *
 * @author Flavio Relvas
 */
public class JpaKitchenLimitRepository extends CafeteriaJpaRepositoryBase<KitchenLimit, Long> implements KitchenLimitRepository {
   
}
