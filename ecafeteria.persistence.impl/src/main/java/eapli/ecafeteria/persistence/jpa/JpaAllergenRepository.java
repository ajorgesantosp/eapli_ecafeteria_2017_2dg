/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.persistence.jpa;

import eapli.ecafeteria.domain.meals.Allergen;
import eapli.ecafeteria.persistence.AllergenRepository;
import eapli.framework.domain.Designation;

/**
 *
 * @author Flavio Relvas
 */
public class JpaAllergenRepository extends CafeteriaJpaRepositoryBase<Allergen, Designation> implements AllergenRepository {

    @Override
    public Allergen findByName(Designation name) {
        return matchOne("e.name.designation='" + name + "'");
    }

}
